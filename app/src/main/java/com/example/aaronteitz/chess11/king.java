package com.example.aaronteitz.chess11;

public class king extends chessPiece{
	public boolean moved;

	public king(int row, int column, String color) {
		super(row, column, color);
		// TODO Auto-generated constructor stub
	}
	public king(int row, int column, String color, boolean moved) {
		super(row, column, color);
		// TODO Auto-generated constructor stub
	}
	/***
	 * move This method allows the King to move within the rules of King. It allows it to eat other pieces as well
	 * @param board
	 * @param i
	 * @param j
	 * @return returns the board after moving the piece
	 */
	@Override
	public chessPiece[][] move(chessPiece[][] board, int i, int j) {
		if(i > 7 || j > 7 || i < 0 || j < 0)
		{
			game.turn = true;
			//System.out.println("Invalid Move");
			return board;
		}

		king K = new king(this.row, this.column, this.color,this.moved);



		if(K.row + 1 == i || K.row - 1 == i || K.row == i )
		{
			if(K.column != j && K.column + 1 != j && K.column- 1 != j)
			{
				rook r = null;
				if( K.column > j)
				{
					r = correctRook(board,i,j-1);
				}
				else
				{
					r = correctRook(board, i, j+1);
				}


				if(r == null)
				{
					game.turn = true;
					return board;
				}

				if(K.moved ==false && r.moved ==false) 
				{

					boolean empty = emptyCheck(board, r.row, r.column, K,r);
					if(empty == true) //swapping items
					{

						//fix the castling
						int a = K.row;
						int b = K.column;
						int z = r.column;
						int y = r.row;
						if(b>z)
						{

							rook g = new rook(K.row,K.column-2,r.color,true);
							board[i][K.column -2] = g;
							K.row = i;
							K.column = j;
							board[i][j] = K;
							K.moved = true;
							g.moved = true;		
							board = fillSpace(board, a, b);
							board = fillSpace(board, K.row, 0);

							inCheck(board, K);
							return board;

						}
						else
						{

							rook g = new rook(K.row,K.column+1,r.color,true);
							board[i][K.column +1] = g;
							K.row = i;
							K.column = j;
							board[i][j] = K;
							K.moved = true;
							g.moved = true;		
							board = fillSpace(board, a, b);
							board = fillSpace(board, K.row, 7);
							inCheck(board, K);
							return board;
						}

					}

					else //meaning that there was stuff in between the rook and the king
					{
						game.turn =true; 
						return board;
					}
					//check if spaces between are empty


				} //they moved 
				game.turn = true;
				//System.out.println("Invalid Move");
				return board;
			}
			if(board[i][j].color != "##" && board[i][j].color != "  ")
			{
				if(board [i][j].color.charAt(0) == K.color.charAt(0))
				{
					rook r = correctRook(board,i,j);

					if(r == null)
					{
						game.turn = true;
						return board;
					}

					if(K.moved ==false && r.moved ==false) 
					{

						boolean empty = emptyCheck(board, r.row, r.column, K,r);
						if(empty == true) //swapping items
						{
							//fix the castling
							int a = K.row;
							int b = K.column;
							int z = r.column;
							int y = r.row;
							if(b>z)
							{

								rook g = new rook(K.row,K.column-2,r.color,true);
								board[i][K.column -2] = g;
								K.row = i;
								K.column = j;
								board[i][j] = K;
								K.moved = true;
								g.moved = true;		
								board = fillSpace(board, a, b);
								board = fillSpace(board, K.row, 0);
								chessPiece [] [] temp = copyThatShit(board);
								if(inCheck(board, K))
								{
									return temp;
								}
								else {
									//inCheck(board, K);
									return board;
								}

							}
							else
							{

								rook g = new rook(K.row,K.column+1,r.color,true);
								board[i][K.column +1] = g;
								K.row = i;
								K.column = j;
								board[i][j] = K;
								K.moved = true;
								g.moved = true;		
								board = fillSpace(board, a, b);
								board = fillSpace(board, K.row, 7);
								inCheck(board, K);
								return board;
							}



						}
						else //meaning that there was stuff in between the rook and the king
						{
							game.turn =true; 
							return board;
						}
						//check if spaces between are empty


					}
					game.turn = true;
					//System.out.println("Invalid Move");
					return board;
				}

			}
			K.moved = true;
			chessPiece [][] tempBoard = copyThatShit(board);
			board = fillSpace(board, K.row, K.column);
			board = replace(board, K, i, j);
			if (inCheck(board, K)){
				return tempBoard;
			}
			else {
				return board;
			}

		}
		game.turn = true;
		//System.out.println("Invalid Move");
		System.out.println();
		return board;
	}
	public static rook correctRook(chessPiece [][] board, int i, int j)  // to find if the piece is a rook
	{

		if((board[i][j].color.charAt(0) == game.w) && (board[i][j].color.charAt(1) == 'R') )
		{
			return (rook)board[i][j];

		}
		return null;
	}
	/***
	 * correctRook checks to see if the castling move finds the correct rook to castle with
	 * @param board
	 * @param i
	 * @param j
	 * @param k
	 * @param r
	 * @return returns the rook piece that is about to be castled
	 */
	public static boolean emptyCheck(chessPiece [][] board, int i, int j, king k, rook r)  //check if the spaces between the rook and king are empty
	{

		if(k.column>r.column)
		{
			if(board[i][j+1].color == "##" || board[i][j+1].color =="  ")
			{
				if(board[i][j+2].color == "##" || board[i][j+2].color =="  ")
				{
					if(board[i][j+3].color == "##" || board[i][j+3].color =="  ")
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;

				}
			}
			else
			{
				return false;
			}

		}
		else if(k.column<r.column)
		{
			if(board[i][j-1].color == "##" || board[i][j-1].color =="  ")
			{
				if(board[i][j-2].color == "##" || board[i][j-2].color =="  ")
				{
					return true;
				}
				else
				{
					return false;

				}
			}
			else
			{
				return false;
			}
		}

		return false;
	}

/***
 * emptyCheck checks to see if the pieces between the rook and King are empty
 * @param board
 * @param i
 * @param j
 * @return returns true if there is nothing between the king and rook
 */
	public static chessPiece [][] fillSpace(chessPiece[][] board, int i, int j){
		if((i%2== 0) && (j%2 == 1 ))  
		{
			board[i][j] = new blank(i,j,"##");
			return board;
		}
		else if((i % 2 == 1) && (j%2 ==0)){
			board[i][j] = new blank(i,j,"##");
			return board;
		}
		else
		{
			board[i][j] = new blank(i,j,"  ");
			return board;
		}
	}
	/***
	 * replace This method replaces the blank/old piece that the King is moving to and returns the board 
	 * @param board
	 * @param K
	 * @param i
	 * @param j
	 * @return returns the chess board after replacing the piece with a King
	 */
	public static chessPiece [][] replace(chessPiece [][] board, king K, int i, int j){
		K.row = i;
		K.column = j;
		board[i][j] = K;
		return board;
	}
	/***
	 * inCheck checks to see if the king's most recent move puts the king in check, if it does it notifies the user via a print statement to the command line
	 * @param board
	 * @param K King 
	 * @return returns a boolean true if the king moved into check and false if the king did not move into check 
	 */
	public static boolean inCheck(chessPiece [][] board, king K)
	{
		boolean check;
		char color = K.color.charAt(0);
		for(int i = 0; i < 8; i++)
		{
			for( int j = 0; j < 8; j++)
			{
				if(board[i][j].color.charAt(0) == color)
				{

				}
				else
				{
					if(board[i][j].color.charAt(1) == 'p') //checking pawns
					{

						if(i + 1 < 8 && j + 1 < 8 && board[i+1][j+1] == K && board[i][j].color == "bp") // check to see if in check by black pawn on right side
						{
							check = true;
							System.out.println("CHECK");
							return check;
						}
						if(i + 1 < 8 && j - 1 >= 0 && board[i+1][j-1] == K && board[i][j].color == "bp") // check to see if in check by black pawn on left side 
						{
							check = true;
							System.out.println("CHECK");
							return check;
						}
						if(i - 1>= 0 && j + 1 < 8 && board[i-1][j+1] == K && board[i][j].color == "wp")
						{
							check = true;
							System.out.println("CHECK");
							return check;
						}
						if(i - 1 >= 0 && j - 1 >= 0 && board[i -1][j-1] == K && board[i][j].color == "wp")
						{
							check = true;
							System.out.println("CHECK");
							return check;
						}
					}
					if(board[i][j].color.charAt(1) == 'R')
					{
						if(K.row != i && K.column != j)
						{

						}
						else
						{
							boolean noCheck = false;
							if(K.row == i && K.column > j) // when the row is the same and it king is at a higher column
							{

								for(int x = j +1; x < K.column; x++)
								{
									if(board[i][x].color != "##" && board[i][x].color != "  ")
									{
										if(board[i][x] == K)
										{
											check = true;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									check = true;
									System.out.println("CHECK");
									return check;
								}
							}
							if(K.row == i && K.column < j) // when the row is the same and the king is at a lower column 
							{
								for(int x = j -1; x > K.column; x --)
								{
									if(board[i][x].color != "##" && board[i][x].color != "  ")
									{
										if(board[i][x] == K)
										{
											check = true;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									check = true;
									System.out.println("CHECK");
									return check;
								}
							}
							if(K.column == j && K.row > i) // King is in the same column as rook and at a higher row 
							{
								for(int x = i +1; x <= K.row ; x++)
								{
									if(board[x][j].color != "##" && board[x][j].color != "  ")
									{
										if(board[x][j] == K)
										{
											System.out.println("CHECK");
											check = true;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									check = true;
									System.out.println("CHECK");
									return check;
								}
							}
							if(K.column == j && K.row < i )
							{
								for(int x = i -1; x > K.row; x--)
								{
									if(board[x][j].color != "##" && board[x][j].color != "  ")
									{
										if(board[x][j] == K)
										{
											check = true;
											return check;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									check = true;
									System.out.println("CHECK");
									return check;
								}
							}
						}
					}
					if(board[i][j].color.charAt(1) == 'Q')
					{
						if(K.row == i || K.column == j) //This case is exactly the same as the rook case;
						{
							boolean noCheck = false;
							if(K.row == i && K.column > j) // when the row is the same and it king is at a higher column
							{

								for(int x = j +1; x < K.column; x++)
								{
									if(board[i][x].color != "##" && board[i][x].color != "  ")
									{
										if(board[i][x] == K)
										{
											check = true;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									check = true;
									System.out.println("CHECK");
									return check;
								}
							}
							if(K.row == i && K.column < j) // when the row is the same and the king is at a lower column 
							{
								for(int x = j -1; x > K.column; x --)
								{
									if(board[i][x].color != "##" && board[i][x].color != "  ")
									{
										if(board[i][x] == K)
										{
											check = true;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									check = true;
									System.out.println("CHECK");
									return check;
								}
							}
							if(K.column == j && K.row > i) // King is in the same column as rook and at a higher row 
							{
								for(int x = i +1; x <= K.row ; x++)
								{
									if(board[x][j].color != "##" && board[x][j].color != "  ")
									{
										if(board[x][j] == K)
										{
											System.out.println("CHECK");
											check = true;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									check = true;
									System.out.println("CHECK");
									return check;
								}
							}
							if(K.column == j && K.row < i )
							{
								for(int x = i -1; x > K.row; x--)
								{
									if(board[x][j].color != "##" && board[x][j].color != "  ")
									{
										if(board[x][j] == K)
										{
											check = true;
											return check;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									check = true;
									System.out.println("CHECK");
									return check;
								}
							}
						}


						int x = K.row - i;
						int y = K.column - j;
						x = Math.abs(x);
						y = Math.abs(y);
						if(x == y) //bishop case
						{
							if(K.row > i && K.column > j) //  king is down and too the right of bishop
							{
								int a;
								int b;
								for(a = i + 1, b = j + 1; b <= K.column && a <= K.row ; a++, b++)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											check = true;
											System.out.println("CHECK!");
											return check;
										}
										else
										{
											check = false;
											break;
										}
									}
								}


							}
							if(K.row < i && K.column > j) //king is up and to the  right
							{
								int a;
								int b;
								for( a = i- 1, b = j + 1; b <= K.column && a >= K.row ; a--, b++)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											check = true;
											System.out.println("CHECK");
											return check;
										}
										else
										{
											check = false;
											break;
										}
									}
								}

							}
							if(K.row > i && K.column < j) // king is down and to the left
							{
								int a;
								int b;
								for(a = i + 1, b = j - 1; b <= j && a >= i ; a++, b--)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											check = true;
											System.out.println("CHECK");
											return check;
										}
										else
										{
											check = false;
											break;
										}
									}
								}
							}
							if(K.row < i && K.column < j) // king is up and to the  left
							{
								int a;
								int b;
								for(a =i - 1, b = j - 1; b >= K.column && a >= K.row ; a--, b--)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											check = true;
											System.out.println("CHECK");
											return check;
										}
										else
										{
											check = false;
											break;
										}
									}
								}							
							}

						}
					}
				}
				if(board[i][j].color.charAt(1) == 'B')
				{
					int x = K.row - i;
					int y = K.column - j;
					x = Math.abs(x);
					y = Math.abs(y);
					if(x!=y) 
					{

					}
					else{

						if(K.row > i && K.column > j) //  king is down and too the right of bishop
						{
							int a;
							int b;
							for(a = i + 1, b = j + 1; b <= K.column && a <= K.row ; a++, b++)
							{
								if(board[a][b].color != "##" && board[a][b].color != "  ")
								{
									if(board[a][b] == K)
									{
										check = true;
										System.out.println("CHECK");
										return check;
									}
									else
									{
										check = false;
										break;
									}
								}
							}


						}
						if(K.row < i && K.column > j) //king is up and to the  right
						{
							int a;
							int b;
							for( a = i- 1, b = j + 1; b <= K.column && a >= K.row ; a--, b++)
							{
								if(board[a][b].color != "##" && board[a][b].color != "  ")
								{
									if(board[a][b] == K)
									{
										check = true;
										System.out.println("CHECK");
										return check;
									}
									else
									{
										check = false;
										break;
									}
								}
							}

						}
						if(K.row > i && K.column < j) // king is down and to the left
						{
							int a;
							int b;
							for(a = i + 1, b = j - 1; b <= j && a >= i ; a++, b--)
							{
								if(board[a][b].color != "##" && board[a][b].color != "  ")
								{
									if(board[a][b] == K)
									{
										check = true;
										System.out.println("CHECK");
										return check;
									}
									else
									{
										check = false;
										break;
									}
								}
							}
						}
						if(K.row < i && K.column < j) // king is up and to the  left
						{
							int a;
							int b;
							for(a =i - 1, b = j - 1; b >= K.column && a >= K.row ; a--, b--)
							{
								if(board[a][b].color != "##" && board[a][b].color != "  ")
								{
									if(board[a][b] == K)
									{
										check = true;
										System.out.println("CHECK");
										return check;
									}
									else
									{
										check = false;
										break;
									}
								}
							}							
						}

					}
					if(board[i][j].color.charAt(1) == 'N')
					{
						if(Math.abs(K.row - i) > 2 || Math.abs(K.column - j) > 2)
						{

						}
						else
						{


							if(i+2 <= 7 && j + 1 <= 7 && board[i+2][j+1] == K)
							{
								check = true;
								System.out.println("CHECK");
								return check;
							}
							if(i+1 <= 7 && j + 2 <= 7 &&board[i + 1][j + 2] == K)
							{
								check = true;
								System.out.println("CHECK");
								return check;
							}
							if(i+2 <= 7 && j - 1 >= 0 && board[i + 2][j - 1] == K)
							{
								check = true;
								System.out.println("CHECK");
								return check;
							}
							if(i+1 <= 7 && j - 2 >= 0 &&board[i + 1][j -2] == K)
							{
								check = true;
								System.out.println("CHECK");
								return check;
							}
							if(i -1 >= 0 && j + 2 <= 7 &&board[i-1][j + 2] == K)
							{
								check = true;
								System.out.println("CHECK");
								return check;
							}
							if(i-1 >= 0 && j - 2 >= 0 &&board[i-1][j - 2] == K)
							{
								check = true;
								System.out.println("CHECK");
								return check;
							}
							if(i-2 >= 0 && j - 1 >= 0 &&board[i-2][j-1] == K)
							{
								check = true;
								System.out.println("CHECK");
								return check;
							}
							if(i - 2 >=0 && j + 1 <=7 && board[i-2][j+1] == K)
							{
								check = true;
								System.out.println("CHECK");
								return check;
							}
						}

					}
				}
			}
		}
		return false;
	}
	public static chessPiece[][] copyThatShit(chessPiece[][] origBoard)
	{
		if (origBoard == null)
			return null;
		chessPiece[][] result = new chessPiece[origBoard.length][];
		for (int r = 0; r < origBoard.length; r++)
		{
			result[r] = origBoard[r].clone();
		}
		return result;
	}

}