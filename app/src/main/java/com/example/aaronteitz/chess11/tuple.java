package com.example.aaronteitz.chess11;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by aaronteitz on 4/23/17.
 */

public class tuple implements Serializable
{
    String origin;

    public chessPiece[][] getCurrBoard() {
        return currBoard;
    }

    public void setCurrBoard(chessPiece[][] currBoard) {
        this.currBoard = currBoard;
    }

    chessPiece [][] currBoard;
    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    String destination;

    public static tuple createTuple(String x, String y, chessPiece [][] board){
        tuple t = new tuple();
        t.setOrigin(x);
        t.setDestination(y);
        t.currBoard = gameView.copyThatShit(board);
        return t;
    }
}
