package com.example.aaronteitz.chess11;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by aaronteitz on 4/22/17.
 */

public class squares extends android.support.v7.widget.AppCompatImageView {
    private chessPiece cp;

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    private boolean selected = false;

    public squares(Context context, chessPiece piece) {
        super(context);
        this.cp = piece;
    }

    public squares(Context context, AttributeSet attrs, chessPiece piece) {
        super(context, attrs);
        this.cp = piece;
    }

    public squares(Context context, AttributeSet attrs, int defStyle, chessPiece piece) {
        super(context, attrs, defStyle);
        this.cp = piece;
    }

    public void setPiece(chessPiece piece) {
        this.cp = piece;
    }

    public chessPiece getPiece() {
        return cp;
    }
}
