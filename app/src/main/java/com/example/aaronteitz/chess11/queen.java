package com.example.aaronteitz.chess11;

public class queen extends chessPiece{

	public queen(int row, int column, String color) {
		super(row, column, color);
		// TODO Auto-generated constructor stub
	}
	/***
	 * move This method allows the Queen to move within the rules of Queen. It allows it to eat other pieces as well
	 * @param board
	 * @param i
	 * @param j
	 * @returns the board after moving the piece
	 */
	@Override
	public chessPiece[][] move(chessPiece[][] board, int i, int j) {
		if((i > 7) || (j >7 )|| (i<0 ) || (j <0))
		{
			game.turn = true;
			//System.out.println("Illegal move, try again");
			return board;
		}
		int x = 0;
		int y =0;
		int a =0;
		int	b =0;
		queen Q = new queen(this.row, this.column, this.color);
		
		
		//need to make for checks, up and right, up and left, down and right, down and left
		
		x = Q.row - i;
		y = Q.column - j;
		x = Math.abs(x);
		y = Math.abs(y);
		if(x == y)
		{
			if( (   (i- Q.row) < 0 ) && ((j-Q.column) > 0) ) 	//up and right
			{
				for(a = Q.row - 1, b = Q.column + 1; b < j && a > i ; a--, b++)
				{
					if(board[a][b].color != "##" && board[a][b].color != "  ")
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				if(board[a][b].color != "##" && board[a][b].color != "  ")
				{
					if(board[a][b].color.charAt(0) == Q.color.charAt(0))
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				board = fillSpace(board, Q.row , Q.column);
				board = replace(board, Q, i, j);
				return board;
			}
			if( (   (i- Q.row) < 0 ) && ((j-Q.column) < 0) ) 	//up and left
			{
				for(a = Q.row - 1, b = Q.column - 1; b > j && a > i ; a--, b--)
				{
					if(board[a][b].color != "##" && board[a][b].color != "  ")
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				if(board[a][b].color != "##" && board[a][b].color != "  ")
				{
					if(board[a][b].color.charAt(0) == Q.color.charAt(0))
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				board = fillSpace(board, Q.row , Q.column);
				board = replace(board, Q, i, j);
				return board;
			}
			if( (   (i- Q.row) > 0 ) && ((j-Q.column) > 0) ) 	//down and right
			{
				for(a = Q.row + 1, b = Q.column + 1; b < j && a > i ; a++, b++)
				{
					if(board[a][b].color != "##" && board[a][b].color != "  ")
					{
						game.turn = true;
					//	System.out.println("Illegal move, try again");
						return board;
					}
				}
				if(board[a][b].color != "##" && board[a][b].color != "  ")
				{
					if(board[a][b].color.charAt(0) == Q.color.charAt(0))
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				board = fillSpace(board, Q.row , Q.column);
				board = replace(board, Q, i, j);
				return board;
			}

			if( (   (i- Q.row) > 0 ) && ((j-Q.column) < 0) ) 	//down and left
			{
				for(a = Q.row + 1, b = Q.column - 1; b < j && a > i ; a++, b--)
				{
					if(board[a][b].color != "##" && board[a][b].color != "  ")
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				if(board[a][b].color != "##" && board[a][b].color != "  ")
				{
					if(board[a][b].color == Q.color)
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				if(board[a][b].color != "##" && board[a][b].color != "  ")
				{
					if(board[a][b].color.charAt(0) == Q.color.charAt(0))
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				board = fillSpace(board, Q.row , Q.column);
				board = replace(board, Q, i, j);
				return board;
			}
			return board;
		}
		else 
		{
			if(Q.row != i && Q.column != j){ //check to see if move is legal
				game.turn = true;
				//System.out.println("Illegal move, try again");
				return board;
			}
			if(Q.row == i && Q.column != j) //move along one row 
			{

				if(Q.column < j){ //for moving right

					for(x = Q.column + 1; x < j; x++)
					{
						if(board[i][x].color != "##" && board[i][x].color != "  ")
						{
							game.turn = true;
							//System.out.println("Illegal move, try again");
							return board;
						}
					}
				}
				if(Q.column > j){ //moving to the left;			
					for(x = Q.column -1; x > j; x--){
						if(board[i][x].color != "##" && board[i][x].color != "  ")
						{
							game.turn = true;
							//System.out.println("Illegal move, try again");
							return board;
						}
					}
				}
				if(board[i][x].color != "##" && board[i][x].color != "  ")
				{
					if(board[i][x].color.charAt(0) == Q.color.charAt(0))
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				board = fillSpace(board, Q.row , Q.column);
				board = replace(board, Q, i, x);
				return board;
			}
			if(Q.column == j && Q.row != i)
			{
				int z = j;
				if(Q.row < i)
				{
					for(z = Q.row + 1; z < i; z++) //moving up
					{
						if(board[z][j].color != "##" && board[z][j].color != "  ")
						{
							game.turn = true;
						//	System.out.println("Illegal move, try again");
							return board;
						}
					}
				}
				if(Q.row > i) //moving down 
				{
					for(z = Q.row - 1; z > i; z --)
					{
						if(board[z][j].color != "##" && board[z][j].color != "  ")
						{
							game.turn = true;
						//	System.out.println("Illegal move, try again");
							return board;
						}	
					}
				}
				if(board[z][j].color != "##" && board[z][j].color != "  ")
				{
					if(board[z][j].color.charAt(0) == Q.color.charAt(0))
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				board = fillSpace(board, Q.row , Q.column);
				board = replace(board, Q, z, j);
				return board;


			}

		}
		
		
		
		return board;
	}
		/***
		 * fillSpace This method fills the space that the Queen moved from
		 * @param board
		 * @param i
		 * @param j
		 * @returns the board after filling the space
		 */
public static chessPiece [][] fillSpace(chessPiece[][] board, int i, int j){
		if((i%2== 0) && (j%2 == 1 ))  
		{
			board[i][j] = new blank(i,j,"##");
			return board;
		}
		else if((i % 2 == 1) && (j%2 ==0)){
			board[i][j] = new blank(i,j,"##");
			return board;
		}
		else
		{
			board[i][j] = new blank(i,j,"  ");
			return board;
		}
	}
/***
 * replace This method replaces the blank/old piece that the Queen is moving to and returns the board
 * @param board
 * @param Q
 * @param i
 * @param j
 * @return returns the board after replacing the piece
 */
public static chessPiece [][] replace(chessPiece [][] board, queen Q, int i, int j){
		Q.row = i;
		Q.column = j;
		board[i][j] = Q;
		return board;
	}


}