package com.example.aaronteitz.chess11;

public class blank extends chessPiece
{

	public blank(int row, int column, String color) 
	{
		super(row, column, color);
		// TODO Auto-generated constructor stub
	}
/***
 *  move This method is to show that a turn did not occur
 * @param board
 * @param i
 * @param j
 *@return returns the board after moving the piece
 */
	@Override
	public chessPiece[][] move(chessPiece[][] board, int i, int j) 
	{
		// TODO Auto-generated method stub
		game.turn = true;
		//System.out.println("Illegal move, try again");
		return board;
	}

}