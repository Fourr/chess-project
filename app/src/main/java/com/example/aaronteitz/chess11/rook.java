package com.example.aaronteitz.chess11;

public class rook extends chessPiece{
	
	public boolean moved;
	
	public rook(int row, int column, String color) 
	{
		super(row, column, color);
		// TODO Auto-generated constructor stub
	}
	public rook(int row, int column, String color, boolean moved)
	{
		super(row, column, color);
		// TODO Auto-generated constructor stub
	}

	@Override
	public chessPiece[][] move(chessPiece[][] board, int i, int j) 
	{
		if(i > 7 || j > 7 || i < 0 || j < 0)
		{
			game.turn = true;
			//System.out.println("Illegal move, try again");
			return board;
		}
		int x = 0;
		rook R = new rook(this.row, this.column, this.color, this.moved);
		if(R.row != i && R.column != j){ //check to see if move is legal
			game.turn = true;
			//System.out.println("Illegal move, try again");
			return board;
		}
		if(R.row == i && R.column != j) //move along one row 
		{

			if(R.column < j){ //for moving right

				for(x = R.column + 1; x < j; x++)
				{
					if(board[i][x].color != "##" && board[i][x].color != "  ")
					{
						game.turn = true;
					//	System.out.println("Illegal move, try again");
						return board;
					}
				}
			}
			if(R.column > j){ //moving to the left;			
				for(x = R.column -1; x > j; x--){
					if(board[i][x].color != "##" && board[i][x].color != "  ")
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
			}
			if(board[i][x].color != "##" && board[i][x].color != "  ")
			{
				if(board[i][x].color.charAt(0) == R.color.charAt(0))
				{
					game.turn = true;
					//System.out.println("Illegal move, try again");
					return board;
				}
			}
			R.moved = true;
			board = fillSpace(board, R.row , R.column);
			board = replace(board, R, i, x);
			return board;
		}
		if(R.column == j && R.row != i)
		{
			int z = j;
			if(R.row < i)
			{
				for(z = R.row + 1; z < i; z++) //moving up
				{
					if(board[z][j].color != "##" && board[z][j].color != "  ")
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
			}
			if(R.row > i) //moving down 
			{
				for(z = R.row - 1; z > i; z --)
				{
					if(board[z][j].color != "##" && board[z][j].color != "  ")
					{
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}	
				}
			}
			if(board[z][j].color != "##" && board[z][j].color != "  ")
			{
				if(board[z][j].color.charAt(0) == R.color.charAt(0))
				{
					game.turn = true;
					//System.out.println("Illegal move, try again");
					return board;
				}
			}
			R.moved = true;
			board = fillSpace(board, R.row , R.column);
			board = replace(board, R, z, j);
			return board;


		}

		return board;
	}

	public static chessPiece [][] fillSpace(chessPiece[][] board, int i, int j){
		if((i%2== 0) && (j%2 == 1 ))  
		{
			board[i][j] = new blank(i,j,"##");
			return board;
		}
		else if((i % 2 == 1) && (j%2 ==0)){
			board[i][j] = new blank(i,j,"##");
			return board;
		}
		else
		{
			board[i][j] = new blank(i,j,"  ");
			return board;
		}
	}

	public static chessPiece [][] replace(chessPiece [][] board, rook R, int i, int j){
		R.row = i;
		R.column = j;
		board[i][j] = R;
		return board;
	}
}