package com.example.aaronteitz.chess11;

import android.widget.Button;
import android.widget.TextView;

import java.util.Scanner;


public class game 
{
	static boolean Checkmate = false;
	static boolean Check = false;
	static char w = 'w';
	static boolean firstTurn = true;
	static boolean turn = false; //to check if a move actually made a move and didnt make an illegal move
	/***
	 * isfirstturn This is the method for the first turn, there are first turn specific things that need to be done.
	 * @param board
	 * @return returns the new correct board with the first move completed
	 */
	public static chessPiece[][] isfirstturn(chessPiece[][] board)
	{

		Scanner scanner = new Scanner(System.in);

		String test = scanner.nextLine();
		int m1 = (test.charAt(0) -97);
		char c = test.charAt(1);
		int m2 = convert((int)c-49);

		int r1 = (test.charAt(3)-97);
		char c2 = test.charAt(4);
		int r2 = convert((int)c2-49);
		if(test.equals("resign") )
		{

			System.out.println("Black wins");
			System.exit(0);
		}
		CharSequence o = "draw?";
		if(test.contains(o))
		{

			if(board[m2][m1].color.charAt(0) == w)
			{
				if(board[m2][m1].color.charAt(1) == 'p')
				{
					board = board[m2][m1].move(board, r2, r1,'p');
				}
				else
				{
					board = board[m2][m1].move(board, r2, r1);

				}


				if(turn == true)
				{
					System.out.println("Illegal move, try again"); 
					return board;
				}


				chess.printBoard(board);
				System.out.println();
				w = 'b';
				String draw = scanner.nextLine();
				if(draw.equals("draw"))
				{
					//System.out.println("draw");
					System.exit(0);
				}

				int q1 = (draw.charAt(0) -97);
				char e = draw.charAt(1);
				int q2 = convert((int)e-49);

				int t1 = (draw.charAt(3)-97);
				char e2 = draw.charAt(4);
				int t2 = convert((int)e2-49);
				if(board[q2][q1].color.charAt(0) == w)
				{

					if(board[m2][m1].color.charAt(1) == 'p')
					{
						board = board[m2][m1].move(board, r2, r1,'p');
					}
					else
					{
						board = board[m2][m1].move(board, r2, r1);

					}
					if(turn == true)
					{
						System.out.println("Illegal move, try again"); 
						return board;
					}
					chess.printBoard(board);
					System.out.println();
					if(w == 'b')
					{
						w = 'w';
						firstTurn = false;
						return board;
					}
					else if(w == 'w')
					{
						w = 'b';
						firstTurn = false;
						return board;
					}

				}
				else
				{
					System.out.println("Illegal move, try again"); 
					return board;
				}

			}
			else
			{
				System.out.println("Illegal move, try again"); 
				return board;
			}
		}

		if(board[m2][m1].color.charAt(0) == w)
		{

			if(board[m2][m1].color.charAt(1) == 'p')
			{
				board = board[m2][m1].move(board, r2, r1,'p');
			}
			else
			{
				board = board[m2][m1].move(board, r2, r1);

			}

			if(turn == true)
			{
				System.out.println("Illegal move, try again"); 
				return board;
			}
			chess.printBoard(board);
			System.out.println();
			if(w == 'b')
			{
				w = 'w';
				firstTurn = false;
				return board;
			}
			else if(w == 'w')
			{
				w = 'b';
				firstTurn = false;
				return board;
			}

		}
		else
		{
			System.out.println("Illegal move, try again");
			return board;
		}
		return board;
	}
	/***
	 * isgame This function is a 'move', it checks for draw, resign, and a move for a piece
	 * @param board
	 */
	public static void isgame (chessPiece[][] board )
	{
		CharSequence o = "draw?";
		Scanner scanner = new Scanner(System.in);
		String test = scanner.nextLine();
		int m1 = (test.charAt(0) -97);
		char c = test.charAt(1);
		int m2 = convert((int)c-49);
		int r1 = (test.charAt(3)-97);
		char c2 = test.charAt(4);
		int r2 = convert((int)c2-49);

		for(int i=0; i <8; i++)
		{
			for(int j = 0; j <8; j++)
			{
				if(board[i][j].color.charAt(0) ==w && board[i][j].color.charAt(1) =='p')
				{
					if( board[i][j].enpassant == true)
					{
						board[i][j].enpassant =false;
					}
				}
			}
		}
		if(test.equals("resign"))
		{
			if(w == 'b')
			{
				System.out.println("White wins");
			}
			if(w == 'w')
			{
				System.out.println("Black wins");
			}

			System.exit(0);
		}

		if(test.contains(o))
		{

			if(board[m2][m1].color.charAt(1) == 'p')
			{

				if(board[m2][m1].color.charAt(0) == w)
				{

					if(test.length() > 10)
					{
						char d = test.charAt(6);
						board = board[m2][m1].move(board, r2, r1, d);

					}
					else
					{
						board = board[m2][m1].move(board, r2, r1);

					}
					if(turn == true)
					{
						System.out.println("Illegal move, try again"); 
						return;
					}
					chess.printBoard(board);
					System.out.println();

					String draw = scanner.nextLine();
					if(draw.equals("draw"))
					{
						//System.out.println("draw");
						System.exit(0);
					}
					if(w == 'b')
					{
						w = 'w';
					}
					else if(w == 'w')
					{
						w = 'b';
					}

					int q1 = (draw.charAt(0) -97);
					char e = draw.charAt(1);
					int q2 = convert((int)e-49);

					int t1 = (draw.charAt(3)-97);
					char e2 = draw.charAt(4);
					int t2 = convert((int)e2-49);
					if(board[q2][q1].color.charAt(0) == w)
					{

						if(draw.length() > 5)
						{
							if(board[q2][q1].color.charAt(1) == 'p')
							{
								char d = draw.charAt(6);
								board = board[q2][q1].move(board, t2, t1,d);

							}
							else
							{
								System.out.println("Illegal move, try again"); 
								return;
							}


						}
						else
						{
							board = board[q2][q1].move(board, t2, t1);


						}
						if(turn == true)
						{
							System.out.println("Illegal move, try again"); 
							return;
						}
						chess.printBoard(board);
						System.out.println();
						if(w == 'b')
						{
							w = 'w';
						}
						else if(w == 'w')
						{
							w = 'b';
						}
						return;
					}
					else
					{
						System.out.println("Illegal move, try again"); 
						return;
					}

				}
				else
				{
					System.out.println("Illegal move, try again"); 
					return;
				}				


			}
			else
			{
				if(board[m2][m1].color.charAt(0) == w)
				{

					if(board[m2][m1].color.charAt(1) == 'p')
					{
						board = board[m2][m1].move(board, r2, r1,'p');
					}
					else
					{
						board = board[m2][m1].move(board, r2, r1);

					}
					if(turn == true)
					{
						System.out.println("Illegal move, try again"); 
						return;
					}
					chess.printBoard(board);
					System.out.println();
					if(w == 'b')
					{
						w = 'w';
					}
					else if(w == 'w')
					{
						w = 'b';
					}
					String draw = scanner.nextLine();
					if(draw.equals("draw"))
					{
						System.out.println("draw");
						System.exit(0);
					}

					int q1 = (draw.charAt(0) -97);
					char e = draw.charAt(1);
					int q2 = convert((int)e-49);

					int t1 = (draw.charAt(3)-97);
					char e2 = draw.charAt(4);
					int t2 = convert((int)e2-49);
					if(board[q2][q1].color.charAt(0) == w)
					{

						if(board[m2][m1].color.charAt(1) == 'p')
						{
							board = board[m2][m1].move(board, r2, r1,'p');
						}
						else
						{
							board = board[m2][m1].move(board, r2, r1);

						}
						if(turn == true)
						{
							System.out.println("Illegal move, try again");
							//TextView tv = (TextView)gameView.findViewById(R.id.textView);

							return;
						}
						chess.printBoard(board);
						System.out.println();
						if(w == 'b')
						{
							w = 'w';
							firstTurn = false;
							return;
						}
						else if(w == 'w')
						{
							w = 'b';
							firstTurn = false;
							return;
						}

					}
					else
					{
						System.out.println("Illegal move, try again"); 
						return;
					}


				}
				else
				{
					System.out.println("Illegal move, try again"); 
					return;
				}



			}
		}

		if(board[m2][m1].color.charAt(1) == 'p')
		{
			if(board[m2][m1].color.charAt(0) == w)
			{
				if(test.length() >5)
				{
					board = board[m2][m1].move(board, r2, r1,test.charAt(6));

				}
				else
				{
					board = board[m2][m1].move(board, r2, r1,'p');

				}
				if(turn == true)
				{
					System.out.println("Illegal move, try again");
					//TextView warningView = (Button)findViewById(R.id.startGame);
					return;
				}
				chess.printBoard(board);
				System.out.println();
				if(w == 'b')
				{
					w = 'w';
					firstTurn = false;
					return;
				}
				else if(w == 'w')
				{
					w = 'b';
					firstTurn = false;
					return;
				}
				return;


			}
			else
			{
				System.out.println("Illegal move, try again");
				return;
			}

		}
		else if(board[m2][m1].color.charAt(0) == w)
		{

			if(board[m2][m1].color.charAt(1) == 'p')
			{
				board = board[m2][m1].move(board, r2, r1,'p');
			}
			else
			{
				board = board[m2][m1].move(board, r2, r1);

			}
			if(turn == true)
			{
				System.out.println("Illegal move, try again"); 
				return;
			}

			chess.printBoard(board);
			System.out.println();
			if(w == 'b')
			{
				w = 'w';
				return;
			}
			else if(w == 'w')
			{
				w = 'b';
				return;
			}


		}
		else
		{
			System.out.println("Illegal move, try again");
			return;
		}

	}
	/***
	 * convert This function converts in input into the correct format for the user
	 * @param board
	 * @return returns the correct value for input
	 */	
public static int convert(int move)
	{
		if( move ==0)
		{
			return 7;
		}
		if(move ==1)
		{
			return 6;
		}
		if( move ==2)
		{
			return 5;
		}
		if( move ==3)
		{
			return 4;
		}
		if( move ==4)
		{
			return 3;
		}
		if( move ==5)
		{
			return 2;
		}
		if( move ==6)
		{
			return 1;
		}
		if( move ==7)
		{
			return 0;
		}
		return 0;

	} 
}