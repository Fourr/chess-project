package com.example.aaronteitz.chess11;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

public class gameView extends AppCompatActivity implements Serializable{
    chessPiece [][] chessBoard = chess.createBoard();
    chessPiece [][] prevBoard = chess.createBoard();
    ArrayList<tuple> moves = new ArrayList<tuple>();

    boolean move = true;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        final int numSelected = 0;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_view);
        chess.setBoard(chessBoard);
        chess.printBoard(chessBoard);
        final TableLayout tl = (TableLayout)findViewById(R.id.board);
        for(int i = 0; i < chessBoard.length; i++)
        {
            TableRow tr = new TableRow(this);
            for(int j = 0; j < chessBoard.length; j++)
            {
                final squares sq = new squares(this, chessBoard[i][j]);
                tr.addView(sq);
                sq.getLayoutParams().height = 70;
                sq.getLayoutParams().width = 70;
                final int x = i;
                final int y = j;
                sq.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {

                        if(sq.isSelected() == true)
                        {
                            sq.setSelected(false);
                            if((x%2== 0) && (y%2 == 1 ))
                            {
                                sq.setBackgroundColor(Color.DKGRAY);
                            }
                            else if((x % 2 == 1) && (y%2 ==0)){
                                sq.setBackgroundColor(Color.DKGRAY);
                            }
                            else
                            {
                                sq.setBackgroundColor(Color.LTGRAY);
                            }
                        }
                        else
                        {
                            if(selectedNum(tl) < 2)
                            {
                                if(selectedNum(tl) == 0 && chessBoard[x][y].getColor() != "  " && chessBoard[x][y].getColor() != "##" )
                                {
                                    sq.setBackgroundColor(Color.YELLOW);
                                    sq.setSelected(true);
                                }
                                else if (selectedNum(tl) > 0) {
                                    sq.setBackgroundColor(Color.YELLOW);
                                    sq.setSelected(true);
                                    int[] originArray = findSelected(tl, x, y);
                                    int originRow = originArray[0];
                                    int originCol = originArray[1];
                                    prevBoard = copyThatShit(chessBoard);
                                    if (chessBoard[originRow][originCol].color.charAt(0) == 'w') {
                                        if (chessBoard[originRow][originCol].color.charAt(1) == 'p') {

                                            chessBoard[originRow][originCol].move(chessBoard, x, y, 'p');
                                            if(isLegalMove()) {
                                                String orig = convertToString(originRow, originCol);
                                                String dest = convertToString(x, y);
                                                tuple t = tuple.createTuple(orig, dest, chessBoard);
                                                moves.add(t);
                                                printMoves(moves);
                                            }
                                            else{
                                                move = false;
                                            }

                                        } else {

                                            chessBoard[originRow][originCol].move(chessBoard, x, y);
                                            if(isLegalMove()) {
                                                String orig = convertToString(originRow, originCol);
                                                String dest = convertToString(x, y);
                                                tuple t = tuple.createTuple(orig, dest, chessBoard);
                                                moves.add(t);
                                                printMoves(moves);
                                            }
                                            else{
                                                move = false;
                                            }
                                        }
                                        //System.out.println("Moving from " +originRow + ", " + originCol + " to " + x + ", " + y);

                                        refresh(chessBoard);
                                        //chess.printBoard(chessBoard);
                                    }
                                    else{
                                        TextView tv = (TextView)findViewById(R.id.textView);
                                        tv.setText("White must move first");

                                        //android.os.SystemClock.sleep(2000);
                                        //tv.setText("");
                                    }
                                }
                            }

                        }
                    }
                });
                if((i%2== 0) && (j%2 == 1 ))
                {
                    sq.setBackgroundColor(Color.DKGRAY);
                }
                else if((i % 2 == 1) && (j%2 ==0)){
                    sq.setBackgroundColor(Color.DKGRAY);
                }
                else
                {
                    sq.setBackgroundColor(Color.LTGRAY);
                }
                if(chessBoard[i][j].getColor().charAt(0) == 'b')
                {
                    if(chessBoard[i][j].getColor().charAt(1) == 'R')
                    {
                        sq.setImageResource(R.drawable.blackrook);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'N')
                    {
                        sq.setImageResource(R.drawable.blackknight);

                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'B')
                    {
                        sq.setImageResource(R.drawable.blackbishop);

                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'Q')
                    {
                        sq.setImageResource(R.drawable.blackqueen);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'K')
                    {
                        sq.setImageResource(R.drawable.blackking);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'p')
                    {
                        sq.setImageResource(R.drawable.blackpawn);
                    }
                }
                if(chessBoard[i][j].getColor().charAt(0) == 'w')
                {
                    if(chessBoard[i][j].getColor().charAt(1) == 'R')
                    {
                        sq.setImageResource(R.drawable.whiterook);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'N')
                    {
                        sq.setImageResource(R.drawable.whiteknight);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'B')
                    {
                        sq.setImageResource(R.drawable.whitebishop);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'Q')
                    {
                        sq.setImageResource(R.drawable.whitequeen);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'K')
                    {
                        sq.setImageResource(R.drawable.whiteking);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'p')
                    {
                        sq.setImageResource(R.drawable.whitepawn);
                    }
                }
            }
            tl.addView(tr);
        }
        Button ai = (Button)findViewById(R.id.AIbutton);
        ai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(move == true)
                {
                    chessBoard = pickMove('w');
                    //move = false;
                }
                else if(move == false){
                    chessBoard = pickMove('b');
                    move = true;
                }
                refresh(chessBoard);
            }
        });
        Button resign = (Button)findViewById(R.id.resignButton);  //Resign Button Implementation  STILL NEED TO IMPLIMENT SAVE HERE
        resign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.gameover);
                dialog.show();
                Button quitGame = (Button)dialog.findViewById(R.id.quitGame);
                quitGame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(gameView.this, MainActivity.class);
                        startActivity(i);
                    }
                });
            }
        });
        Button save = (Button)findViewById(R.id.backButton);
        System.out.println("WHY WONT THIS PRINT");
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("WE MADE IT before SERIAL");
                AlertDialog.Builder builder = new AlertDialog.Builder(gameView.this);
                final EditText et = new EditText(gameView.this);
                builder.setView(et);
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                    }
                });
                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String x = et.getText().toString();
                        serialList saveList = new serialList();
                        saveList.setName(x);
                        saveList.setGames(moves);
                        saveList.setDate(Calendar.getInstance());
                        serialList.serial(saveList, gameView.this);

                        // FIRE ZE MISSILES!
                    }
                });
                builder.setMessage("Input the name for your game");

                AlertDialog d = builder.create();
                builder.show();

            }
        });
        Button draw = (Button)findViewById(R.id.drawButton); //Draw Implementation
        draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.popupview);
                dialog.show();
                Button accept = (Button)dialog.findViewById(R.id.AcceptDraw);

                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                Button decline = (Button)dialog.findViewById(R.id.declineDraw);
                decline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
            }
        });
    }

    public void squareClick(View view)
    {
        switch (view.getId())
        {
        }

    }

    public int selectedNum(TableLayout tl) {
        int numSelected = 0;
        for (int i = 0, j = tl.getChildCount(); i < j; i++)
        {
            View view = tl.getChildAt(i);
            if (view instanceof TableRow)
            {
                // then, you can remove the the row you want...
                // for instance...
                TableRow row = (TableRow) view;
                //System.out.println(tl.getChildAt(i).isSelected() + " <------    i = :" + i);
                for(int x = 0; x < 8; x++)
                {
                    if (((TableRow) view).getChildAt(x).isSelected() == true)
                    {
                        System.out.println("GOT HERE");
                        numSelected++;
                    }
                }
            }
        }
        return numSelected;
    }
    public int [] findSelected(TableLayout tl, int currRow, int currCol) {
        int [] selected = new int [2];
        System.out.println("currRow = : " +currRow + "currCol = : " + currCol);
        for (int i = 0, j = tl.getChildCount(); i < j; i++)
        {
            View view = tl.getChildAt(i);
            if (view instanceof TableRow)
            {
                // then, you can remove the the row you want...
                // for instance...
                TableRow row = (TableRow) view;
                //System.out.println(tl.getChildAt(i).isSelected() + " <------    i = :" + i);
                for(int x = 0; x < 8; x++)
                {
                    if (((TableRow) view).getChildAt(x).isSelected() == true)
                    {
                        System.out.println("WE ARE IN HERE BITCHES" + i + "   " + x);
                        if(i != currRow || x != currCol)
                        {
                            selected[0] = i;
                            selected[1] = x;
                            //System.out.println("i = : " + i + "x = : " + x + " <-----------------");

                            return selected;
                        }
                    }
                }
            }
        }
       // System.out.println("SELECTED AT 0 = " + selected[0]);
        //System.out.println("SELECTED AT 1 = " + selected[1]);

        return selected;
    }
    public void refresh(final chessPiece [][] currBoard)
    {
        final int numSelected = 0;
        setContentView(R.layout.activity_game_view);
        king kw = chess.findWhiteKing(chessBoard);
        if(chess.inCheck(chessBoard, kw, 'w'))
        {
            if(move == true)
            {
                chessBoard = copyThatShit(prevBoard);
                if(moves.size() > 2) {
                    prevBoard = copyThatShit(moves.get(moves.size() - 2).getCurrBoard());
                }
                moves.remove(moves.size()-1);
                move = false;
                refresh(chessBoard);
                return;

            }
            if(chess.isCheckMate(chessBoard, kw)){
                TextView tv = (TextView)findViewById(R.id.textView);
                tv.setText("Check Mate, Black Wins");
                Dialog dialog2 = new Dialog(this);
                dialog2.setContentView(R.layout.gameover);
                dialog2.show();
                Button saveGame = (Button)dialog2.findViewById(R.id.saveGame);
                //System.out.println("SDFSFSFSDFSF");
                saveGame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(gameView.this);
                        final EditText et = new EditText(gameView.this);
                        builder.setView(et);
                        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String x = et.getText().toString();
                                serialList saveList = new serialList();
                                saveList.setName(x);
                                saveList.setGames(moves);
                                saveList.setDate(Calendar.getInstance());
                                serialList.serial(saveList, gameView.this);

                                // FIRE ZE MISSILES!
                            }
                        });
                        builder.setMessage("Input the name for your game");

                        AlertDialog d = builder.create();
                        builder.show();

                    }
                });
                Button quitGame = (Button)dialog2.findViewById(R.id.quitGame);
                quitGame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(gameView.this, MainActivity.class);
                        startActivity(i);
                    }
                });
                //Intent i = new Intent(gameView.this, endGame.class);
                //startActivity(i);
            }
            else {
                TextView tv = (TextView) findViewById(R.id.textView);
                tv.setText("White King in Check");
            }
        }
        king kb = chess.findBlackKing(chessBoard);
        if(chess.inCheck(chessBoard, kb, 'b')){
            if(move == false)
            {
                chessBoard = copyThatShit(prevBoard);
                if(moves.size() > 2) {
                    prevBoard = copyThatShit(moves.get(moves.size() - 2).getCurrBoard());
                }
                moves.remove(moves.size()-1);
                move = true;
                refresh(chessBoard);
                return;

            }
            if(chess.isCheckMate(chessBoard, kb)){
                TextView tv = (TextView)findViewById(R.id.textView);
                tv.setText("Check Mate, White Wins");
                Dialog dialog2 = new Dialog(this);
                dialog2.setContentView(R.layout.gameover);
                dialog2.show();
                Button saveGame = (Button)dialog2.findViewById(R.id.saveGame);
                //System.out.println("SDFSFSFSDFSF");
                saveGame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(gameView.this);
                        final EditText et = new EditText(gameView.this);
                        builder.setView(et);
                        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String x = et.getText().toString();
                                serialList saveList = new serialList();
                                saveList.setName(x);
                                saveList.setGames(moves);
                                saveList.setDate(Calendar.getInstance());
                                serialList.serial(saveList, gameView.this);

                                // FIRE ZE MISSILES!
                            }
                        });
                        builder.setMessage("Input the name for your game");

                        AlertDialog d = builder.create();
                        builder.show();

                    }
                });
                Button quitGame = (Button)dialog2.findViewById(R.id.quitGame);
                quitGame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(gameView.this, MainActivity.class);
                        startActivity(i);
                    }
                });
                Intent i = new Intent(gameView.this, endGame.class);
               // startActivity(i);
            }
            else {
                System.out.println("WE GOT HERE");
                TextView tv = (TextView) findViewById(R.id.textView);
                tv.setText("Black King in Check");
            }
        }
        Button save = (Button)findViewById(R.id.backButton);
        System.out.println("WHY WONT THIS PRINT");
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("WE MADE IT before SERIAL");
                AlertDialog.Builder builder = new AlertDialog.Builder(gameView.this);
                final EditText et = new EditText(gameView.this);
                builder.setView(et);
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                    }
                });
                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String x = et.getText().toString();
                        serialList saveList = new serialList();
                        saveList.setName(x);
                        saveList.setGames(moves);
                        saveList.setDate(Calendar.getInstance());
                        serialList.serial(saveList, gameView.this);

                        // FIRE ZE MISSILES!
                    }
                });
                builder.setMessage("Input the name for your game");

                AlertDialog d = builder.create();
                builder.show();

            }
        });
        Button undo = (Button)findViewById(R.id.forwardMove);
        undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("WE MADE IT BOI");
                if(!chessBoard.equals(prevBoard)) {
                    chessBoard = copyThatShit(prevBoard);
                    chessBoard = copyThatShit(moves.get(moves.size()-2).getCurrBoard());

                    System.out.println();
                    //chess.printBoard(prevBoard);
                    System.out.println();
                    //System.out.println("*****************");
                    deleteMove(moves);
                    printMoves(moves);
                    if(move == false){
                        move = true;
                    }
                    else if(move == true){
                        move = false;
                    }
                    System.out.println("WE ARE GETTING HERE");
                    chess.printBoard(chessBoard);
                    System.out.println("************************");
                    chess.printBoard(prevBoard);
                    refresh(chessBoard);
                }
                if(chessBoard.equals(prevBoard) && moves.size() > 2){
                    chessBoard = copyThatShit(moves.get(moves.size()-2).getCurrBoard());
                    System.out.println("JLKSJDFL:DSJFLKSJDFLKJSD:LKFSDLFJLFJLJLKJFLKSJF:JKLFLFJL:JF");
                    refresh(chessBoard);
                }
            }
        });
        Button ai = (Button)findViewById(R.id.AIbutton);
        ai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(move == true)
                {
                    chessBoard = pickMove('b');
                    move = false;
                }
                else if(move == false){
                    chessBoard = pickMove('w');
                    move = true;
                }
                refresh(chessBoard);
            }
        });
        Button resign = (Button)findViewById(R.id.resignButton);  //Resign Button Implementation STILL NEED TO IMPLEMENT SAVE!!!!!
        resign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.gameover);
                dialog.show();

                Button quitGame = (Button)dialog.findViewById(R.id.quitGame);
                quitGame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(gameView.this, MainActivity.class);
                        startActivity(i);
                    }
                });
                Button save = (Button)dialog.findViewById(R.id.saveGame);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(gameView.this);
                        final EditText et = new EditText(gameView.this);
                        builder.setView(et);
                        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String x = et.getText().toString();
                                serialList saveList = new serialList();
                                saveList.setName(x);
                                saveList.setGames(moves);
                                saveList.setDate(Calendar.getInstance());
                                serialList.serial(saveList, gameView.this);

                                // FIRE ZE MISSILES!
                            }
                        });
                        builder.setMessage("Input the name for your game");

                        AlertDialog d = builder.create();
                        builder.show();

                    }
                });
            }
        });
        Button draw = (Button)findViewById(R.id.drawButton);
        draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.popupview);
                dialog.show();
                Button accept = (Button)dialog.findViewById(R.id.AcceptDraw);

                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Dialog dialog2 = new Dialog(v.getContext());
                        dialog2.setContentView(R.layout.gameover);
                        dialog2.show();
                        Button save = (Button)dialog2.findViewById(R.id.saveGame);
                        save.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(gameView.this);
                                final EditText et = new EditText(gameView.this);
                                builder.setView(et);
                                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                });
                                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        String x = et.getText().toString();
                                        serialList saveList = new serialList();
                                        saveList.setName(x);
                                        saveList.setGames(moves);
                                        saveList.setDate(Calendar.getInstance());
                                        serialList.serial(saveList, gameView.this);

                                        // FIRE ZE MISSILES!
                                    }
                                });
                                builder.setMessage("Input the name for your game");

                                AlertDialog d = builder.create();
                                builder.show();

                            }
                        });
                        Button quitGame = (Button)dialog2.findViewById(R.id.quitGame);
                        quitGame.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(gameView.this, MainActivity.class);
                                startActivity(i);
                            }
                        });
                    }
                });
                Button decline = (Button)dialog.findViewById(R.id.declineDraw);
                decline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
            }
        });
        final TableLayout tl = (TableLayout)findViewById(R.id.board);
        for(int i = 0; i < currBoard.length; i++)
        {
            TableRow tr = new TableRow(this);
            for(int j = 0; j < currBoard.length; j++)
            {
                final squares sq = new squares(this, currBoard[i][j]);
                tr.addView(sq);
                sq.getLayoutParams().height = 70;
                sq.getLayoutParams().width = 70;
                final int x = i;
                final int y = j;
                sq.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {

                        if(sq.isSelected() == true)
                        {
                            sq.setSelected(false);
                            if((x%2== 0) && (y%2 == 1 ))
                            {
                                sq.setBackgroundColor(Color.DKGRAY);
                            }
                            else if((x % 2 == 1) && (y%2 ==0)){
                                sq.setBackgroundColor(Color.DKGRAY);
                            }
                            else
                            {
                                sq.setBackgroundColor(Color.LTGRAY);
                            }
                        }
                        else
                        {
                            if(selectedNum(tl) < 2)
                            {
                                if(selectedNum(tl) == 0 && chessBoard[x][y].getColor() != "  " && chessBoard[x][y].getColor() != "##" )
                                {
                                    sq.setBackgroundColor(Color.YELLOW);
                                    sq.setSelected(true);
                                }
                                else if (selectedNum(tl) > 0) {
                                    sq.setBackgroundColor(Color.YELLOW);
                                    sq.setSelected(true);
                                    int[] originArray = findSelected(tl, x, y);
                                    int originRow = originArray[0];
                                    int originCol = originArray[1];
                                    chessPiece[][] board2 = chess.createBoard();
                                    prevBoard = copyThatShit(chessBoard);
                                    chess.printBoard(prevBoard);
                                    System.out.println();
                                    System.out.println("-------------------------");
                                    if ((move == true && chessBoard[originRow][originCol].color.charAt(0) == 'b') || (move == false && chessBoard[originRow][originCol].color.charAt(0) == 'w')) {

                                        if (chessBoard[originRow][originCol].color.charAt(1) == 'p') {
                                            if((x == 0 && chessBoard[originRow][originCol].color.charAt(0) == 'w') ||( x == 7 && chessBoard[originRow][originCol].color.charAt(0) == 'b')){
                                                promotionDialog(v, originRow, originCol, x , y);
                                                return;
                                            }

                                            chessBoard[originRow][originCol].move(chessBoard, x, y, 'p');
                                            if(isLegalMove() == true) {
                                                if (move == true) {
                                                    move = false;
                                                } else if (move == false) {
                                                    move = true;
                                                }
                                                String orig = convertToString(originRow, originCol);
                                                String dest = convertToString(x, y);
                                                tuple t = tuple.createTuple(orig, dest, chessBoard);
                                                moves.add(t);
                                                printMoves(moves);
                                            }
                                            else{
                                                prevBoard = copyThatShit(moves.get(moves.size()-1).getCurrBoard());
                                            }

                                            //chess.printBoard(chessBoard);


                                        } else {

                                            chessBoard[originRow][originCol].move(chessBoard, x, y);

                                                System.out.println("GETTING HERE");
                                            if(isLegalMove() == true) {
                                                if (move == true) {
                                                    move = false;
                                                } else if (move == false) {
                                                    move = true;
                                                }
                                                String orig = convertToString(originRow, originCol);
                                                String dest = convertToString(x, y);
                                                tuple t = tuple.createTuple(orig, dest, chessBoard);
                                                moves.add(t);
                                                printMoves(moves);
                                            }
                                            else{
                                                prevBoard = copyThatShit(moves.get(moves.size()-2).getCurrBoard());
                                                System.out.println("We Are Here");
                                                chess.printBoard(moves.get(moves.size()-2).getCurrBoard());
                                            }

                                        }
                                        System.out.println("Moving from " + originRow + ", " + originCol + " to " + x + ", " + y);
                                        refresh(chessBoard);
                                        //chess.printBoard(chessBoard);
                                    }
                                }
                                else{
                                    if(move == true){
                                        TextView tv = (TextView)findViewById(R.id.textView);
                                        tv.setText("It is Black's Turn");
                                    }
                                    if(move == false){
                                        TextView tv = (TextView)findViewById(R.id.textView);
                                        tv.setText("It is White's Turn");
                                    }
                                }
                            }

                        }
                    }
                });
                if((i%2== 0) && (j%2 == 1 ))
                {
                    sq.setBackgroundColor(Color.DKGRAY);
                }
                else if((i % 2 == 1) && (j%2 ==0)){
                    sq.setBackgroundColor(Color.DKGRAY);
                }
                else
                {
                    sq.setBackgroundColor(Color.LTGRAY);
                }
                if(chessBoard[i][j].getColor().charAt(0) == 'b')
                {
                    if(chessBoard[i][j].getColor().charAt(1) == 'R')
                    {
                        sq.setImageResource(R.drawable.blackrook);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'N')
                    {
                        sq.setImageResource(R.drawable.blackknight);

                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'B')
                    {
                        sq.setImageResource(R.drawable.blackbishop);

                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'Q')
                    {
                        sq.setImageResource(R.drawable.blackqueen);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'K')
                    {
                        sq.setImageResource(R.drawable.blackking);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'p')
                    {
                        sq.setImageResource(R.drawable.blackpawn);
                    }
                }
                if(chessBoard[i][j].getColor().charAt(0) == 'w')
                {
                    if(chessBoard[i][j].getColor().charAt(1) == 'R')
                    {
                        sq.setImageResource(R.drawable.whiterook);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'N')
                    {
                        sq.setImageResource(R.drawable.whiteknight);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'B')
                    {
                        sq.setImageResource(R.drawable.whitebishop);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'Q')
                    {
                        sq.setImageResource(R.drawable.whitequeen);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'K')
                    {
                        sq.setImageResource(R.drawable.whiteking);
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'p')
                    {
                        sq.setImageResource(R.drawable.whitepawn);
                    }
                }
            }
            tl.addView(tr);
        }
    }
    public static chessPiece[][] copyThatShit(chessPiece[][] origBoard)
    {
        if (origBoard == null)
            return null;
        chessPiece[][] result = new chessPiece[origBoard.length][];
        for (int r = 0; r < origBoard.length; r++)
        {
            result[r] = origBoard[r].clone();
        }
        return result;
    }

    public String convertToString(int row, int col)
    {
        String move = "";
        if(col == 0)
        {
            move = move + "a";
        }
        if(col == 1){
            move = move + "b";
        }
        if(col ==2){
            move = move + "c";

        }
        if(col == 3){
            move = move + "d";
        }
        if(col == 4){
            move = move + "e";
        }
        if(col == 5){
            move = move + "f";
        }
        if(col == 6){
            move = move + "g";
        }
        if(col == 7){
            move = move + "h";
        }
        if(row == 0)
        {
            move = move + "8";
        }
        if(row == 1){
            move = move + "7";
        }
        if(row ==2){
            move = move + "6";
        }
        if(row == 3){
            move = move + "5";
        }
        if(row == 4){
            move = move + "4";
        }
        if(row == 5){
            move = move + "3";
        }
        if(row == 6){
            move = move + "2";
        }
        if(row == 7){
            move = move + "1";
        }
        System.out.println("THE MOVE IS   " + move);
        return move;
    }
    public static void printMoves(ArrayList<tuple> moves){
        System.out.println();
        System.out.println("THE MOVES ARRAY LOOKS AS FOLLOWS");

        for(int i = 0; i < moves.size(); i++){
            System.out.println();
            System.out.println(moves.get(i).getOrigin() + "  " + moves.get(i).getDestination());
            System.out.println();
            chess.printBoard(moves.get(i).getCurrBoard());
            System.out.println();
        }
    }
    public void deleteMove(ArrayList<tuple> moves){
        moves.remove(moves.size()-1);
    }
    public boolean isLegalMove(){
        boolean legal = false;
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if(!chessBoard[i][j].getColor().equals(prevBoard[i][j].getColor())){
                    return true;
                }
            }
        }
        return legal;
    }
    public chessPiece [][] pickMove(char color)
    {
        prevBoard = copyThatShit(chessBoard);
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++){
                if(chessBoard[i][j].getColor().charAt(0) == color)
                {
                    if(chessBoard[i][j].getColor().charAt(1) == 'p'){
                        if(chessBoard[i][j].getColor().charAt(0) == 'b')
                        {
                            chessBoard = chessBoard[i][j].move(chessBoard, i+1, j, 'p');
                            System.out.println("1");
                            if(isLegalMove()){
                                String orig = convertToString(i, j);
                                String dest = convertToString(i+1, j);
                                tuple t = tuple.createTuple(orig, dest, chessBoard);
                                moves.add(t);
                                return chessBoard;
                            }
                        }
                        if(chessBoard[i][j].getColor().charAt(0) == 'w'){

                            chessBoard[i][j].move(chessBoard, i-1, j, 'p');
                            System.out.println("2");
                            if(isLegalMove()){
                                String orig = convertToString(i, j);
                                String dest = convertToString(i-1, j);
                                tuple t = tuple.createTuple(orig, dest, chessBoard);
                                moves.add(t);
                                return chessBoard;
                            }
                        }
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'N')
                    {
                        System.out.println("3");
                        chessBoard[i][j].move(chessBoard, i+2, j+1);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i+2, j+1);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i+1, j+2);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i+1, j+2);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i-2, j-1);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i-2, j-1);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i-1, j-2);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i-1, j-2);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i-2, j+1);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i-2, j+1);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i-1, j+2);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i-1, j+2);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i+1, j-2);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i+1, j-2);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i+2, j-1);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i+2, j-1);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'Q' || chessBoard[i][j].getColor().charAt(1) == 'K'|| chessBoard[i][j].getColor().charAt(1) == 'R'){
                        //FINISH THIS
                        System.out.println("a");
                        chess.printBoard(chessBoard);

                        System.out.println("KKKKKKKKDKDKKDKDKDKDKKKDK");
                        chessBoard[i][j].move(chessBoard, i+1, j);
                        chess.printBoard(chessBoard);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i+1, j);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            System.out.println("A");
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i , j+1);
                        if(isLegalMove()){
                            System.out.println("b");
                            String orig = convertToString(i, j);
                            String dest = convertToString(i, j+1);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i-1, j);
                        if(isLegalMove()){
                            System.out.println("c");
                            String orig = convertToString(i, j);
                            String dest = convertToString(i-1, j);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i, j-1);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i, j-1);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            System.out.println("d");

                            return chessBoard;
                        }
                    }
                    if(chessBoard[i][j].getColor().charAt(1) == 'B'){
                        System.out.println("5");
                        chessBoard[i][j].move(chessBoard, i+1, j+1);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i+1, j+1);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i+1, j-1);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i+1, j-1);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i-1, j +1);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i-1, j+1);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                        chessBoard[i][j].move(chessBoard, i-1, j-1);
                        if(isLegalMove()){
                            String orig = convertToString(i, j);
                            String dest = convertToString(i-1, j-1);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            return chessBoard;
                        }
                    }
                }
            }
        }
        System.out.println("WE MADE IT OVEER HERE!!");
        return chessBoard;
    }
    public void showAlertDialog(View v)
    {
        new AlertDialog.Builder(v.getContext())
                .setTitle("Delete entry")
                .setMessage("Are you sure you want to delete this entry?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    public void promotionDialog(View v, final int originRow, final int originCol, final int destRow, final int destCol)
    {
        String [] promo = {"Queen", "Knight", "Rook", "Bishop"};
        String pick_color = "What type of promotion would you like?";
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setTitle(pick_color)
                .setItems(promo, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0){
                            chessBoard[originRow][originCol].move(chessBoard, destRow, destCol, 'q');
                        }
                        if(which == 1){
                            chessBoard[originRow][originCol].move(chessBoard, destRow, destCol, 'n');
                        }
                        if(which == 2){
                            chessBoard[originRow][originCol].move(chessBoard, destRow, destCol, 'r');
                        }
                        if(which == 3){
                            chessBoard[originRow][originCol].move(chessBoard, destRow, destCol, 'b');
                        }
                        chessBoard[originRow][originCol].move(chessBoard, destRow, destCol, 'p');
                        if(isLegalMove() == true) {
                            if (move == true) {
                                move = false;
                            } else if (move == false) {
                                move = true;
                            }
                            String orig = convertToString(originRow, originCol);
                            String dest = convertToString(destRow, destCol);
                            tuple t = tuple.createTuple(orig, dest, chessBoard);
                            moves.add(t);
                            printMoves(moves);

                        }
                        else{
                            prevBoard = copyThatShit(moves.get(moves.size()-1).getCurrBoard());
                        }
                        refresh(chessBoard);

                    }
                });
                builder.show();
        //return which;
    }


}
