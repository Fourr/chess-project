package com.example.aaronteitz.chess11;

import java.io.Serializable;

/***
 * ChessPiece contains all of the getters and setters for a generic piece. This class is the parent class for all other pieces on the board 
 * 
 *
 */
public abstract class chessPiece implements Serializable {

	
	
	
	
	boolean causingCheck = false;
	public boolean enpassant;
	
	int row;
	int column;
	String color;
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getColumn() {
		return column;
	}
	public void setColumn(int column) {
		this.column = column;
	}
	public chessPiece(int row, int column, String color) {
		this.row = row;
		this.column = column; 
		this.color = color;
	}
	public abstract chessPiece[][] move(chessPiece [][] board, int i, int j);
	public chessPiece[][] move(chessPiece[][] board, int rowMove, int colMove,char g) {
		// TODO Auto-generated method stub
		return null;
	}
}