package com.example.aaronteitz.chess11;

public class bishop extends chessPiece{

	public bishop(int row, int column, String color) {
		super(row, column, color);
		// TODO Auto-generated constructor stub
	}

	/***
	 * move This method allows the bishop to move within the rules of bishop. It allows it to eat other pieces as well
	 * @param board
	 * @param i
	 * @param j
	 * @return returns the board after moving the piece
	 */
	@Override
	public chessPiece[][] move(chessPiece[][] board, int i, int j) 
	{
		if((i > 7) || (j >7 )|| (i<0 ) || (j <0))
		{
			System.out.println();
			game.turn = true;
			//System.out.println("Illegal move, try again");
			return board;
		}
		int x = 0;
		int y =0;
		int a =0;
		int	b =0;
		bishop B = new bishop(this.row, this.column, this.color);
		
		
		//need to make for checks, up and right, up and left, down and right, down and left
		
		x = B.row - i;
		y = B.column - j;
		x = Math.abs(x);
		y = Math.abs(y);
		if(x!=y) 				// basically check if the move they make can be done by a bishop
		{
			System.out.println();
			game.turn = true;
			//System.out.println("Illegal move, try again");
			
			return board;
		}
		
		
			if( (   (i- B.row) < 0 ) && ((j-B.column) > 0) ) 	//up and right
			{
				for(a = B.row - 1, b = B.column + 1; b < j && a > i ; a--, b++)
				{
					if(board[a][b].color != "##" && board[a][b].color != "  ")
					{
						System.out.println();
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				if(board[a][b].color != "##" && board[a][b].color != "  ")
				{
					if(board[a][b].color.charAt(0) == B.color.charAt(0))
					{
						//System.out.println();
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				board = fillSpace(board, B.row , B.column);
				board = replace(board, B, i, j);
				return board;
			}
			if( (   (i- B.row) < 0 ) && ((j-B.column) < 0) ) 	//up and left
			{
				for(a = B.row - 1, b = B.column - 1; b > j && a > i ; a--, b--)
				{
					if(board[a][b].color != "##" && board[a][b].color != "  ")
					{
						//System.out.println();
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				if(board[a][b].color != "##" && board[a][b].color != "  ")
				{
					if(board[a][b].color.charAt(0) == B.color.charAt(0))
					{
						//System.out.println();
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				board = fillSpace(board, B.row , B.column);
				board = replace(board, B, i, j);
				return board;
			}
			if( (   (i- B.row) > 0 ) && ((j-B.column) > 0) ) 	//down and right
			{
				for(a = B.row + 1, b = B.column + 1; b < j && a < i ; a++, b++)
				{
					if(board[a][b].color != "##" && board[a][b].color != "  ")
					{
					//	System.out.println();
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				if(board[a][b].color != "##" && board[a][b].color != "  ")
				{
					if(board[a][b].color.charAt(0) == B.color.charAt(0))
					{
						//System.out.println();
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				board = fillSpace(board, B.row , B.column);
				board = replace(board, B, i, j);
				return board;
			}

			if( (   (i- B.row) > 0 ) && ((j-B.column) < 0) ) 	//down and left
			{
				for(a = B.row + 1, b = B.column - 1; b < j && a > i ; a++, b--)
				{
					if(board[a][b].color != "##" && board[a][b].color != "  ")
					{
						//System.out.println();
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				if(board[a][b].color != "##" && board[a][b].color != "  ")
				{
					if(board[a][b].color == B.color)
					{
						//System.out.println();
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				if(board[a][b].color != "##" && board[a][b].color != "  ")
				{
					if(board[a][b].color.charAt(0) == B.color.charAt(0))
					{
						//System.out.println();
						game.turn = true;
						//System.out.println("Illegal move, try again");
						return board;
					}
				}
				board = fillSpace(board, B.row , B.column);
				board = replace(board, B, i, j);
				return board;
			}
			return board;
	}
	
	/***
	 * fillSpace This method fills the space that the bishop moved from
	 * @param board
	 * @param i
	 * @param j
	 * @return returns the board after filling the space
	 */
	public static chessPiece [][] fillSpace(chessPiece[][] board, int i, int j)

	{
		if((i%2== 0) && (j%2 == 1 ))  
		{
			board[i][j] = new blank(i,j,"##");
			return board;
		}
		else if((i % 2 == 1) && (j%2 ==0)){
			board[i][j] = new blank(i,j,"##");
			return board;
		}
		else
		{
			board[i][j] = new blank(i,j,"  ");
			return board;
		}
	}
	/***
	 * replace This method replaces the blank/old piece that the bishop is moving to and returns the board
	 * @param board
	 * @param B
	 * @param i
	 * @param j
	 * @return returns the board after replacing the piece
	 */
	public static chessPiece [][] replace(chessPiece [][] board, bishop B, int i, int j)
	{
		B.row = i;
		B.column = j;
		board[i][j] = B;
		return board;
	}
	

}