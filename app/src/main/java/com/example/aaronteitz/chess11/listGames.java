package com.example.aaronteitz.chess11;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class listGames extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //System.out.println("SDFLJS:FJLK:SDJF:KLSDJFL:KJSDF:J");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_games);

        final ListView lv = (ListView) findViewById(R.id.gameList);
        final ArrayList<serialList> displayList = serialList.unserial(listGames.this);
        // Instanciating an array list (you don't need to do this,
        // you already have yours).
        final List<String> your_array_list = new ArrayList<String>();
        for(int i = 0; i < displayList.size(); i++){
            String name = displayList.get(i).getName();
            Calendar date = displayList.get(i).getDate();
            String display = date.get(Calendar.MONTH) + " / " + date.get(Calendar.DAY_OF_MONTH) + " / " + date.get(Calendar.YEAR);;
            String displayString = name + ":" + "   " + display;
            your_array_list.add(displayString);



        }

        // This is the array adapter, it takes the context of the activity as a
        // first parameter, the type of list view as a second parameter and your
        // array as a third parameter.
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                your_array_list );

        lv.setAdapter(arrayAdapter);
        //arrayAdapter.notifyDataSetChanged();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int j = 0; j < lv.getChildCount(); j++)
                {
                    view.setSelected(false);
                    lv.getChildAt(j).setBackgroundColor(Color.TRANSPARENT);
                    lv.getChildAt(j).setSelected(true);
                    // change the background color of the selected element
                    view.setBackgroundColor(Color.LTGRAY);
                    //view.setSelected(true);
                    if(view.isSelected())
                    {
                        System.out.println("GOT HERE ");
                        System.out.println(arrayAdapter.getItem(j));
                        System.out.println(j);
                        String justName = getName(arrayAdapter.getItem(j));
                        for(int i = 0; i < displayList.size(); i++)
                        {
                            if(justName.equals(displayList.get(i).getName()))
                            {
                                System.out.println("here");
                                ArrayList<tuple> serList = new ArrayList<tuple>();
                                serList = displayList.get(i).getGames();
                                System.out.println(serList.get(0).getCurrBoard());
                                Intent intent = new Intent(listGames.this, playbackGame.class);
                                intent.putExtra("save", i);
                                startActivity(intent);
                                //playbackGame.playGame(serlist);
                            }
                        }
                    }

                }
            }
        });
        final Button sortByDate = (Button)findViewById(R.id.sortByDate);
        sortByDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortByDatefx(your_array_list);
                arrayAdapter.notifyDataSetChanged();
            }
        });
        final Button sortByName = (Button)findViewById(R.id.sortByName);
        sortByName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("WE made it here");
                sortByNamefx(your_array_list);
                arrayAdapter.notifyDataSetChanged();
            }
        });
        Button home = (Button)findViewById(R.id.homeButton);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(listGames.this, MainActivity.class);
                //intent.putExtra("save", i);
                startActivity(intent);
            }
        });
    }
    public String getName(String fullString)
    {
        String justName = "";

        for(int i = 0; i < fullString.length(); i++){
            if(fullString.charAt(i) == ':'){
                justName = fullString.substring(0,i);
                System.out.println(justName);
            }
        }
        return justName;
    }
    public void sortByDatefx(List lv){
        Collections.sort(lv, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                int x = getDates(o1);
                int y = getDates(o2);
                x = x+3;
                y = y +3;
                System.out.println(o1.substring(x, o1.length()));
                System.out.println(o2.substring(y, o2.length()));
                return o1.substring(x, o1.length()).compareTo(o2.substring(y, o2.length()));
            }
        });

    }
    public void sortByNamefx(final List lv){

        Collections.sort(lv, new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                System.out.println("_________________");
                return o1.compareTo(o2);
            }
        });
    }

    public int getDates(String x){
        int y;

        y = x.indexOf(':');
        return y;
    }

}
