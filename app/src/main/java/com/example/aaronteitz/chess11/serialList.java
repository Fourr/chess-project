package com.example.aaronteitz.chess11;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by aaronteitz on 4/25/17.
 */

public class serialList implements Serializable{
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public ArrayList<tuple> getGames() {
        return games;
    }

    public void setGames(ArrayList<tuple> games) {
        this.games = games;
    }

    String name;
    Calendar date;
    ArrayList<tuple> games;
    public static boolean serial(serialList s, Context c)
    {
        try
        {
            ArrayList<serialList> sers = unserial(c);
            if(sers.size() > 1) {
                for (int i = 0; i < sers.size(); i++) {
                    if (s.getName().equals(sers.get(i).getName())) {
                        return false;
                    }
                }
            }
            sers.add(s);

            FileOutputStream fos = c.openFileOutput("save.ser", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(sers);
            os.close();
            fos.close();
            System.out.println("SUCCESS!!");
            return true;
        }
        catch (IOException e)
        {
            System.out.println("NOT SERIALIZING");
            return false;
        }
    }
    public static ArrayList<serialList> unserial(Context c)
    {
        try
        {
            FileInputStream fis = c.openFileInput("save.ser");
            ObjectInputStream is = new ObjectInputStream(fis);
            ArrayList<serialList> save = (ArrayList<serialList>) is.readObject();
            is.close();
            fis.close();
            System.out.println("ALSO SUCCESS!");
            return save;
        }
        catch (ClassNotFoundException | IOException e)
        {
            System.out.println("NOT UNSERIALIZING");
            return new ArrayList<serialList>();
        }

    }
    public static boolean deleteFromSerial(serialList s, Context c)
    {
        try
        {
            ArrayList<serialList> sers2 = unserial(c);
            ArrayList<serialList> sers = new ArrayList<serialList>();
            if(sers2.size() >= 1) {
                for (int i = 0; i < sers2.size(); i++) {
                    System.out.println("THIS IS RETARDED");
                    System.out.println(s.getName() + "   <---- S" + "   " + sers2.get(i).getName() + "  " + "sers at i");
                    if (s.getName().equals(sers2.get(i).getName())) {
                        System.out.println("GETTING HERE");
                    }
                    else{

                        sers.add(s);
                    }
                }
            }

            FileOutputStream fos = c.openFileOutput("save.ser", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(sers);
            os.close();
            fos.close();
            System.out.println("SUCCESS!!");
            return true;
        }
        catch (IOException e)
        {
            System.out.println("NOT SERIALIZING");
            return false;
        }
    }
}
