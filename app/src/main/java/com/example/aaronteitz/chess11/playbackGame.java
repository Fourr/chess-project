package com.example.aaronteitz.chess11;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by aaronteitz on 4/26/17.
 */

public class playbackGame  extends AppCompatActivity {
    int i = 0;
    int x;
    ArrayList<tuple> moves;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playback);
        Intent i = getIntent();
        x = i.getExtras().getInt("save");
        moves = serialList.unserial(playbackGame.this).get(x).getGames();
        gameView.printMoves(moves);
        playGame(moves, x);
        chessPiece [][] currBoard = chess.createBoard();
        chess.setBoard(currBoard);
        buildBoard(currBoard);

    }
    public void playGame(ArrayList<tuple>  s, int x)
    {
        final ArrayList<tuple> s1 = s;

        Button forward = (Button)findViewById(R.id.forwardMove);
        final int z = x;
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("WE MADE IT BOIIII");
                if(i < s1.size()) {
                    refresh(s1.get(i).getCurrBoard());
                    i++;
                }
                else {
                    TextView tv = (TextView) findViewById((R.id.textView));
                    tv.setText("END OF GAME");
                }
            }
        });
        Button delete = (Button)findViewById(R.id.delete);
        final int deleteThis = x;
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serialList.deleteFromSerial(serialList.unserial(playbackGame.this).get(deleteThis), playbackGame.this);
                    Intent intent = new Intent(playbackGame.this, listGames.class);
                    //intent.putExtra("save", i);
                    startActivity(intent);
            }
        });
        Button backwards = (Button)findViewById(R.id.backButton);
        //final int z = x;
        backwards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i--;
                if(i-1 >= 0){
                    refresh(s1.get(i-1).getCurrBoard());
                }
                else if(i == 0)
                {
                    chessPiece [][] currBoard = chess.createBoard();
                    chess.setBoard(currBoard);
                    refresh(currBoard);                }
                else if(i == -1){
                   TextView tv = (TextView)findViewById(R.id.textView);
                    tv.setText("AT BEGINNING OF GAME");
                    i++;

                }
            }
        });
    }
    public void buildBoard(chessPiece [][] chessBoard)
    {
            // System.out.println("REFRESH IS CALLED");
            final int numSelected = 0;
            //super.onCreate(savedInstanceSate);
            final TableLayout tl = (TableLayout)findViewById(R.id.board);
            for(int i = 0; i < chessBoard.length; i++)
            {
                TableRow tr = new TableRow(this);
                for(int j = 0; j < chessBoard.length; j++)
                {
                    final squares sq = new squares(this, chessBoard[i][j]);
                    tr.addView(sq);
                    sq.getLayoutParams().height = 70;
                    sq.getLayoutParams().width = 70;
                    final int x = i;
                    final int y = j;
                    sq.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v)
                        {

                            if(sq.isSelected() == true)
                            {
                                sq.setSelected(false);
                                if((x%2== 0) && (y%2 == 1 ))
                                {
                                    sq.setBackgroundColor(Color.DKGRAY);
                                }
                                else if((x % 2 == 1) && (y%2 ==0)){
                                    sq.setBackgroundColor(Color.DKGRAY);
                                }
                                else
                                {
                                    sq.setBackgroundColor(Color.LTGRAY);
                                }
                            }

                        }
                    });
                    if((i%2== 0) && (j%2 == 1 ))
                    {
                        sq.setBackgroundColor(Color.DKGRAY);
                    }
                    else if((i % 2 == 1) && (j%2 ==0)){
                        sq.setBackgroundColor(Color.DKGRAY);
                    }
                    else
                    {
                        sq.setBackgroundColor(Color.LTGRAY);
                    }
                    if(chessBoard[i][j].getColor().charAt(0) == 'b')
                    {
                        if(chessBoard[i][j].getColor().charAt(1) == 'R')
                        {
                            sq.setImageResource(R.drawable.blackrook);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'N')
                        {
                            sq.setImageResource(R.drawable.blackknight);

                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'B')
                        {
                            sq.setImageResource(R.drawable.blackbishop);

                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'Q')
                        {
                            sq.setImageResource(R.drawable.blackqueen);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'K')
                        {
                            sq.setImageResource(R.drawable.blackking);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'p')
                        {
                            sq.setImageResource(R.drawable.blackpawn);
                        }
                    }
                    if(chessBoard[i][j].getColor().charAt(0) == 'w')
                    {
                        if(chessBoard[i][j].getColor().charAt(1) == 'R')
                        {
                            sq.setImageResource(R.drawable.whiterook);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'N')
                        {
                            sq.setImageResource(R.drawable.whiteknight);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'B')
                        {
                            sq.setImageResource(R.drawable.whitebishop);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'Q')
                        {
                            sq.setImageResource(R.drawable.whitequeen);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'K')
                        {
                            sq.setImageResource(R.drawable.whiteking);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'p')
                        {
                            sq.setImageResource(R.drawable.whitepawn);
                        }
                    }
                }
                tl.addView(tr);
            }
        }
        public void refresh(chessPiece [][] chessBoard){


            // System.out.println("REFRESH IS CALLED");
            final int numSelected = 0;
            //super.onCreate(savedInstanceState);
            setContentView(R.layout.playback);


            final TableLayout tl = (TableLayout)findViewById(R.id.board);
            for(int i = 0; i < chessBoard.length; i++)
            {
                TableRow tr = new TableRow(this);
                for(int j = 0; j < chessBoard.length; j++)
                {
                    final squares sq = new squares(this, chessBoard[i][j]);
                    tr.addView(sq);
                    sq.getLayoutParams().height = 70;
                    sq.getLayoutParams().width = 70;
                    final int x = i;
                    final int y = j;

                    if((i%2== 0) && (j%2 == 1 ))
                    {
                        sq.setBackgroundColor(Color.DKGRAY);
                    }
                    else if((i % 2 == 1) && (j%2 ==0)){
                        sq.setBackgroundColor(Color.DKGRAY);
                    }
                    else
                    {
                        sq.setBackgroundColor(Color.LTGRAY);
                    }
                    if(chessBoard[i][j].getColor().charAt(0) == 'b')
                    {
                        if(chessBoard[i][j].getColor().charAt(1) == 'R')
                        {
                            sq.setImageResource(R.drawable.blackrook);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'N')
                        {
                            sq.setImageResource(R.drawable.blackknight);

                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'B')
                        {
                            sq.setImageResource(R.drawable.blackbishop);

                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'Q')
                        {
                            sq.setImageResource(R.drawable.blackqueen);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'K')
                        {
                            sq.setImageResource(R.drawable.blackking);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'p')
                        {
                            sq.setImageResource(R.drawable.blackpawn);
                        }
                    }
                    if(chessBoard[i][j].getColor().charAt(0) == 'w')
                    {
                        if(chessBoard[i][j].getColor().charAt(1) == 'R')
                        {
                            sq.setImageResource(R.drawable.whiterook);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'N')
                        {
                            sq.setImageResource(R.drawable.whiteknight);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'B')
                        {
                            sq.setImageResource(R.drawable.whitebishop);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'Q')
                        {
                            sq.setImageResource(R.drawable.whitequeen);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'K')
                        {
                            sq.setImageResource(R.drawable.whiteking);
                        }
                        if(chessBoard[i][j].getColor().charAt(1) == 'p')
                        {
                            sq.setImageResource(R.drawable.whitepawn);
                        }
                    }
                }

                tl.addView(tr);
                //playGame(serialList.unserial(playbackGame.this).get(z).getGames(), z);
                playGame(moves, x);
            }
        }
        public void deleteGame(int x){
//            final ArrayList<serialList> displayList = serialList.unserial(playbackGame.this);
//
//            displayList.remove(x);
//            serialList.serial(null, playbackGame.this);
//                    Intent intent = new Intent(playbackGame.this, listGames.class);
//                    //intent.putExtra("save", i);
//                    startActivity(intent);
//                    //playbackGame.playGame(serlist);



        }


}
