package com.example.aaronteitz.chess11;

public class pawn extends chessPiece {


	public char promotion = 'q';



	public pawn(int row, int column, String color) {
		super(row, column, color);
		// TODO Auto-generated constructor stub
	}
	public pawn(int row, int column, String color, char promotion) {
		super(row, column, color);
	}
	/***
	 * move This method allows the pawn to move within the rules of pawn. It allows it to eat other pieces as well. It also has the functionality of enPassant and promotion
	 * @param board
	 * @param
	 * @param
	 * @return returns the board after moving/promoting the piece
	 */
	@Override
	/***
	 * move This method allows the pawn to move within the rules of pawn. It allows it to eat other pieces as well. This method also includes the features of promotion and enPassant.
	 * @param board
	 * @param i
	 * @param j
	 * @return board
	 */
	public chessPiece [][] move(chessPiece [][] board, int rowMove, int colMove, char g){

		if(rowMove > 7 || colMove > 7 || rowMove < 0 || colMove < 0)
		{			
			game.turn = true;
			//System.out.println("Invalid Move");
			return board;
		}
		//Fill in legal pawn move
		pawn P = new pawn(this.row, this.column, this.color, this.promotion);
		P.promotion = g;
		if(this.column != colMove && (board[rowMove][colMove].color == "##" || board[rowMove][colMove].color == "  "))
		{
			if(this.column + 1 == colMove && this.row + 1 == rowMove)
			{
				if(board[this.row][this.column+1].color !=this.color && board[this.row][this.column+1].color.charAt(1) == 'p' && board[this.row][this.column+1].enpassant == true)
				{

					board = fillSpace(board, this.row, this.column);	
					P.row = rowMove;
					P.column = colMove;

					board = fillSpace(board,this.row,this.column+1);
					board[rowMove][colMove] = P;
					return board;

				}
			}
			if(this.column - 1 == colMove && this.row + 1 == rowMove)
			{
				if(board[this.row][this.column-1].color !=this.color && board[this.row][this.column-1].color.charAt(1) == 'p' && board[this.row][this.column-1].enpassant == true)
				{

					board = fillSpace(board, this.row, this.column);	
					P.row = rowMove;
					P.column = colMove;

					board = fillSpace(board,this.row,this.column-1);
					board[rowMove][colMove] = P;
					return board;

				}
			}
			if(this.column - 1 == colMove && this.row - 1 == rowMove)
			{
				if(board[this.row][this.column-1].color !=this.color && board[this.row][this.column-1].color.charAt(1) == 'p' && board[this.row][this.column-1].enpassant == true)
				{

					board = fillSpace(board, this.row, this.column);	
					P.row = rowMove;
					P.column = colMove;

					board = fillSpace(board,this.row,this.column-1);
					board[rowMove][colMove] = P;
					return board;

				}
			}
			if(this.column + 1 == colMove && this.row - 1 == rowMove)
			{
				if(board[this.row][this.column+1].color !=this.color && board[this.row][this.column+1].color.charAt(1) == 'p' && board[this.row][this.column+1].enpassant == true)
				{

					board = fillSpace(board, this.row, this.column);	
					P.row = rowMove;
					P.column = colMove;

					board = fillSpace(board,this.row,this.column+1);
					board[rowMove][colMove] = P;
					return board;

				}
			}
		}
		if(this.column != colMove && (board[rowMove][colMove].color != "##" && board[rowMove][colMove].color != "  "))
		{

			if(this.column + 1 == colMove && this.row + 1 == rowMove)
			{

				if(board[rowMove][colMove].color != this.color)
				{
					board = fillSpace(board, this.row, this.column);	
					P.row = rowMove;
					P.column = colMove;
					if(rowMove == 7 || rowMove == 0)
					{
						if(P.promotion == 'q')
						{
							char x = P.color.charAt(0);
							String y = x + "Q";
							queen Q = new queen(P.row, P.column, y);
							board[rowMove][colMove] = Q;
							return board;
						}
						if(P.promotion == 'n')
						{
							char x = P.color.charAt(0);
							String y = x + "N";
							knight N = new knight(P.row, P.column, y);
							board[rowMove][colMove] = N;
							return board;
						}
						if(P.promotion == 'b')
						{
							char x = P.color.charAt(0);
							String y = x + "B";
							bishop B = new bishop(P.row, P.column, y);
							board[rowMove][colMove] = B;
							return board;
						}
						if(P.promotion == 'r')
						{
							char x = P.color.charAt(0);
							String y = x + "R";
							rook R = new rook(P.row, P.column, y);
							board[rowMove][colMove] = R;
							return board;
						}
					}
					board[rowMove][colMove] = P;
					return board;
				}
				game.turn = true;
				//System.out.println("Invalid Move");
				return board;
			}
			if(this.column +1 == colMove && this.row - 1 == rowMove)
			{

				if(board[rowMove][colMove].color != this.color)
				{
					board = fillSpace(board, this.row, this.column);   
					P.row = rowMove;
					P.column = colMove;
					if(rowMove == 7 || rowMove == 0)
					{
						if(P.promotion == 'q')
						{
							char x = P.color.charAt(0);
							String y = x + "Q";
							queen Q = new queen(P.row, P.column, y);
							board[rowMove][colMove] = Q;
							return board;
						}
						if(P.promotion == 'n')
						{
							char x = P.color.charAt(0);
							String y = x + "N";
							knight N = new knight(P.row, P.column, y);
							board[rowMove][colMove] = N;
							return board;
						}
						if(P.promotion == 'b')
						{
							char x = P.color.charAt(0);
							String y = x + "B";
							bishop B = new bishop(P.row, P.column, y);
							board[rowMove][colMove] = B;
							return board;
						}
						if(P.promotion == 'r')
						{
							char x = P.color.charAt(0);
							String y = x + "R";
							rook R = new rook(P.row, P.column, y);
							board[rowMove][colMove] = R;
							return board;
						}
					}
					board[rowMove][colMove] = P;
					return board;
				}
			}
			if(this.column - 1 == colMove && this.row + 1 == rowMove)
			{
				if(board[this.row+1][this.column].color !=this.color && board[this.row+1][this.column].color.charAt(1) == 'p' && board[this.row+1][this.column].enpassant == true)
				{

					board = fillSpace(board, this.row, this.column);	
					P.row = rowMove;
					P.column = colMove;

					board = fillSpace(board,this.row+1,this.column);
					board[rowMove][colMove] = P;
					return board;

				}
				if(board[rowMove][colMove].color != this.color){
					board = fillSpace(board, this.row, this.column);   
					P.row = rowMove;
					P.column = colMove;
					if(rowMove == 7 || rowMove == 0)
					{
						if(P.promotion == 'q')
						{
							char x = P.color.charAt(0);
							String y = x + "Q";
							queen Q = new queen(P.row, P.column, y);
							board[rowMove][colMove] = Q;
							return board;
						}
						if(P.promotion == 'n')
						{
							char x = P.color.charAt(0);
							String y = x + "N";
							knight N = new knight(P.row, P.column, y);
							board[rowMove][colMove] = N;
							return board;
						}
						if(P.promotion == 'b')
						{
							char x = P.color.charAt(0);
							String y = x + "B";
							bishop B = new bishop(P.row, P.column, y);
							board[rowMove][colMove] = B;
							return board;
						}
						if(P.promotion == 'r')
						{
							char x = P.color.charAt(0);
							String y = x + "R";
							rook R = new rook(P.row, P.column, y);
							board[rowMove][colMove] = R;
							return board;
						}
					}
					board[rowMove][colMove] = P;
					return board;
				}
			}
			if(this.column - 1 == colMove && this.row - 1 == rowMove)
			{
				if(board[this.row-1][this.column].color !=this.color && board[this.row-1][this.column].color.charAt(1) == 'p' && board[this.row-1][this.column].enpassant == true)
				{

					board = fillSpace(board, this.row, this.column);	
					P.row = rowMove;
					P.column = colMove;

					board = fillSpace(board,this.row-1,this.column);
					board[rowMove][colMove] = P;
					return board;

				}
				if(board[rowMove][colMove].color != this.color)
				{
					board = fillSpace(board, this.row, this.column);   
					P.row = rowMove;
					P.column = colMove;
					if(rowMove == 7 || rowMove == 0)
					{
						if(P.promotion == 'q')
						{
							char x = P.color.charAt(0);
							String y = x + "Q";
							queen Q = new queen(P.row, P.column, y);
							board[rowMove][colMove] = Q;
							return board;
						}
						if(P.promotion == 'n')
						{
							char x = P.color.charAt(0);
							String y = x + "N";
							knight N = new knight(P.row, P.column, y);
							board[rowMove][colMove] = N;
							return board;
						}
						if(P.promotion == 'b')
						{
							char x = P.color.charAt(0);
							String y = x + "B";
							bishop B = new bishop(P.row, P.column, y);
							board[rowMove][colMove] = B;
							return board;
						}
						if(P.promotion == 'r')
						{
							char x = P.color.charAt(0);
							String y = x + "R";
							rook R = new rook(P.row, P.column, y);
							board[rowMove][colMove] = R;
							return board;
						}
					}
					board[rowMove][colMove] = P;
					return board;
				}
			}
			game.turn = true;
			//System.out.println("Invalid Move");
			return board;
		}
		if(this.column != colMove)
		{
			game.turn = true;
			//System.out.print("Invalid Move");
			return board;
		}
		if(this.color == "bp" && rowMove < this.row){ //check to see that move is in correct direction for black
			game.turn = true;
			//System.out.println("Invalid Move");
			return board;
		}
		if(this.color == "wp" && rowMove > this.row){ //check to see that move is in correct direction for white 
			game.turn = true;
			//System.out.println("Invalid Move");
			return board;
		}
		if((this.row == 1 && rowMove == 3) || (this.row == 6 && rowMove == 4)){ //If a pawn is in the starting position, it can move two spaces
			if(rowMove == 3)
			{ //check to see if anything is in the way of a move on the white side
				if(board[this.row +1][this.column].color == "##" || board[this.row +1][this.column].color == "  ")
				{
					if(board[this.row +2][this.column].color == "##" || board[this.row +2][this.column].color == "  ")
					{
						board = fillSpace(board, this.row, this.column);	
						P.row = rowMove;
						P.enpassant = true;
						board[rowMove][colMove] = P;
						return board;
					}
				}
			}
			if(rowMove == 4) //check to see if anything is in the way of an initial move on the white side 
			{
				if(board[P.row - 1][P.column].color == "##" || board[P.row - 1][P.column].color == "  ")
				{
					if(board[P.row -2][P.column].color == "##" || board[P.row -2][P.column].color == "  ")
					{
						board = fillSpace(board, P.row, P.column);
						P.row = rowMove;
						P.enpassant = true;
						board[rowMove][colMove] = P;
						return board;
					}				
				}
			}
		}
		if(P.color == "bp"){
			if(P.row + 1 == rowMove)
			{
				if(board[rowMove][colMove].color == "##" || board[rowMove][colMove].color == "  ")//move to a space without a piece in it for black 
				{
					board = fillSpace(board, P.row, P.column);
					P.row = rowMove;
					board[rowMove][colMove] = P;
					if(rowMove == 7 || rowMove == 0)
					{
						if(P.promotion == 'q')
						{
							char x = P.color.charAt(0);
							String y = x + "Q";
							queen Q = new queen(P.row, P.column, y);
							board[rowMove][colMove] = Q;
							return board;
						}
						if(P.promotion == 'n')
						{
							char x = P.color.charAt(0);
							String y = x + "N";
							knight N = new knight(P.row, P.column, y);
							board[rowMove][colMove] = N;
							return board;
						}
						if(P.promotion == 'b')
						{
							char x = P.color.charAt(0);
							String y = x + "B";
							bishop B = new bishop(P.row, P.column, y);
							board[rowMove][colMove] = B;
							return board;
						}
						if(P.promotion == 'r')
						{
							char x = P.color.charAt(0);
							String y = x + "R";
							rook R = new rook(P.row, P.column, y);
							board[rowMove][colMove] = R;
							return board;
						}
					}
					return board;
				}

			}
			game.turn = true;
			System.out.println("Invalid Move");
		}
		if(P.color == "wp"){
			if(P.row - 1 == rowMove){
				if(board[rowMove][colMove].color == "##" || board[rowMove][colMove].color == "  ")//move to a space without a piece in it for white
				{ 
					board = fillSpace(board, P.row, P.column);
					P.row = rowMove;
					board[rowMove][colMove] = P;
					if(rowMove == 7 || rowMove == 0)
					{
						if(P.promotion == 'q')
						{
							char x = P.color.charAt(0);
							String y = x + "Q";
							queen Q = new queen(P.row, P.column, y);
							board[rowMove][colMove] = Q;
							return board;
						}
						if(P.promotion == 'n')
						{
							char x = P.color.charAt(0);
							String y = x + "N";
							knight N = new knight(P.row, P.column, y);
							board[rowMove][colMove] = N;
							return board;
						}
						if(P.promotion == 'b')
						{
							char x = P.color.charAt(0);
							String y = x + "B";
							bishop B = new bishop(P.row, P.column, y);
							board[rowMove][colMove] = B;
							return board;
						}
						if(P.promotion == 'r')
						{
							char x = P.color.charAt(0);
							String y = x + "R";
							rook R = new rook(P.row, P.column, y);
							board[rowMove][colMove] = R;
							return board;
						}
					}
					return board;
				}


			}
			game.turn = true;
			//System.out.println("Invalid Move");
		}

		return board;
	}
	/***
	 * fillSpace This method fills the space that the pawn moved from
	 * @param board
	 * @param i
	 * @param j
	 * @return returns the board after moving a pawn
	 */
	public static chessPiece [][] fillSpace(chessPiece[][] board, int i, int j){
		if((i%2== 0) && (j%2 == 1 ))  
		{
			board[i][j] = new blank(i,j,"##");
		}
		else if((i % 2 == 1) && (j%2 ==0)){
			board[i][j] = new blank(i,j,"##");
		}
		else
		{
			board[i][j] = new blank(i,j,"  ");
		}
		return board;
	}
	@Override

	public chessPiece[][] move(chessPiece[][] board, int i, int j) 
	{
		// TODO Auto-generated method stub
		return null;
	}
}