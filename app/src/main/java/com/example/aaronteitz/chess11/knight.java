package com.example.aaronteitz.chess11;

public class knight extends chessPiece{

	public knight(int row, int column, String color) 
	{
		super(row, column, color);
		// TODO Auto-generated constructor stub
	}
	/***
	 * move This method allows the knight to move within the rules of knight. It allows it to eat other pieces as well
	 * @param board
	 * @param i
	 * @param j
	 * @return the board after moving the knight
	 */
	@Override
	public chessPiece[][] move(chessPiece[][] board, int i, int j) {
		knight K = new knight(this.row, this.column, this.color);
		if(i > 7 || j > 7 || i < 0 || j < 0)
		{
			game.turn = true;
			//System.out.println("Illegal move, try again");
			return board;
		}
		if(i == K.row + 2 || i == K.row -2)
		{
			if(j != K.column + 1 && j != K.column - 1)
			{
				game.turn = true;
			//	System.out.println("Illegal move, try again");
				return board;
			}
			if(board[i][j].color != "##" && board[i][j].color != "  ")
			{
				if(board [i][j].color.charAt(0) == K.color.charAt(0))
				{
					game.turn = true;
				//	System.out.println("Illegal move, try again");
					return board;
				}
				board = fillSpace(board, K.row, K.column);
				board = replace(board, K, i, j);
				return board;
			}
			else
			{
				board = fillSpace(board, K.row, K.column);
				board = replace(board, K, i, j);
				return board;
			}
		}
		if(i == K.row + 1 || i == K.row -1)
		{
			if(j != K.column + 2 && j != K.column - 2)
			{
				game.turn = true;
				//System.out.println("Illegal move, try again");
				return board;
			}
			if(board[i][j].color != "##" && board[i][j].color != "  ")
			{
				if(board [i][j].color.charAt(0) == K.color.charAt(0))
				{
					game.turn = true;
					//System.out.println("Illegal move, try again");
					return board;
				}
				board = fillSpace(board, K.row, K.column);
				board = replace(board, K, i, j);
				return board;
			}
			else
			{
				board = fillSpace(board, K.row, K.column);
				board = replace(board, K, i, j);
				return board;
			}
		}
		if(j == K.column + 2 || j == K.column - 2)
		{
			if(i != K.row + 1 && i != K.row -1)
			{
				game.turn = true;
				//System.out.println("Illegal move, try again");
				return board;
			}
			if(board[i][j].color != "##" && board[i][j].color != "  ")
			{
				if(board [i][j].color.charAt(0) == K.color.charAt(0))
				{
					game.turn = true;
				//	System.out.println("Illegal move, try again");
					return board;
				}
				board = fillSpace(board, K.row, K.column);
				board = replace(board, K, i, j);
				return board;
			}
			else
			{
				board = fillSpace(board, K.row, K.column);
				board = replace(board, K, i, j);
				return board;
			}
		}
		if(j == K.column + 1 || j == K.column - 1)
		{
			if(i != K.row + 2 && i != K.row -2)
			{
				game.turn = true;
				//System.out.println("Illegal move, try again");
				return board;
			}
			if(board[i][j].color != "##" && board[i][j].color != "  ")
			{
				if(board [i][j].color.charAt(0) == K.color.charAt(0))
				{
					game.turn = true;
					//System.out.println("Illegal move, try again");
					return board;
				}
				board = fillSpace(board, K.row, K.column);
				board = replace(board, K, i, j);
				return board;
			}
			else
			{
				board = fillSpace(board, K.row, K.column);
				board = replace(board, K, i, j);
				return board;
			}
		}
		game.turn = true;
		//System.out.println("Illegal move, try again");
		return board;

	}
/***
 * fillSpace This method fills the space that the knight moved from
 * @param board
 * @param i
 * @param j
 * @return returns the board after filling the space
 */
public static chessPiece [][] fillSpace(chessPiece[][] board, int i, int j){
		if((i%2== 0) && (j%2 == 1 ))  
		{
			board[i][j] = new blank(i,j,"##");
			return board;
		}
		else if((i % 2 == 1) && (j%2 ==0)){
			board[i][j] = new blank(i,j,"##");
			return board;
		}
		else
		{
			board[i][j] = new blank(i,j,"  ");
			return board;
		}
	}
/***
 * replace This method replaces the blank/old piece that the knight is moving to and returns the board
 * @param board
 * @param K
 * @param i
 * @param j
 * @return returns the board after replacing the piece
 */
public static chessPiece [][] replace(chessPiece [][] board, knight K, int i, int j){
		K.row = i;
		K.column = j;
		board[i][j] = K;
		return board;
	}

}