package com.example.aaronteitz.chess11;

import java.io.Serializable;

public class chess implements Serializable
{	
	/**
	 * Main Method used to set the initial board, print the board, and start the game. 
	 * @param args
	 */
	public static void main(String args[])
	{
		chessPiece [][] board = createBoard();
		setBoard(board);
		printBoard(board);
		System.out.println();
		System.out.println();
		System.out.println();
		while(game.firstTurn == true)
		{
			game.turn =false;
			game.isfirstturn(board);
			System.out.println();
			System.out.println();
		}
		while(game.Checkmate == false)
		{

			game.turn = false;
			game.isgame(board);

			king k = findWhiteKing(board);
			if(inCheck(board, k, 'w'))
			{
				System.out.println("Check");
			}
			game.Check = inCheck(board, k, 'w');
			if(game.Check == true)
			{
				if(isCheckMate(board, k) ==true && blockCheck(board, k) == true && killCheck(board) == false)
					//if(isCheckMate(board, k) ==true)
				{
					System.out.println("Checkmate");
					System.out.println("Black wins");
					System.exit(0);
				}

			}
			king l = findBlackKing(board);
			if(inCheck(board, l, 'b'))
			{
				System.out.println("Check");
			}
			game.Check = inCheck(board, l, 'b');
			if(game.Check == true)
			{
				if(isCheckMate(board, l) ==true && blockCheck(board, l) == true && killCheck(board) == false)
				{
					System.out.println("Checkmate");
					System.out.println("White wins");
					System.exit(0);

				}

			}

			System.out.println();
			System.out.println();

		}

	}
	/***
	 * print board iterates through the two dimensional array (the board passed in) and prints out the "color" field of each piece. 
	 * @param board
	 */
	public static void printBoard(chessPiece[][] board){
		for(int i = 0; i <= 8; i ++)
		{
			System.out.println();
			for(int j = 0; j <= 8; j++)
			{
				System.out.print(board[i][j].getColor() + " ");
			}
		}
	}
	/***
	 * Create Board initalizes a new 9 X 9 array to be used as the chess board and populates the spaces with either "##" for black squares and "  " for white squares
	 * Performs basic math to decide whether a square should be black or white. 
	 * @return Returns a board with all pieces filled with "blanks"
	 */
	public static chessPiece[][] createBoard()
	{
		chessPiece [][] board = new chessPiece [9][9];
		for(int i = 0; i <= 7; i++)
		{
			for(int j = 0; j <= 7; j++)
			{
				if((i%2== 0) && (j%2 == 1 ))  
				{
					board[i][j] = new blank(i,j,"##");
				}
				else if((i % 2 == 1) && (j%2 ==0)){
					board[i][j] = new blank(i,j,"##");
				}
				else
				{
					board[i][j] = new blank(i,j,"  ");
				}
			}
		}
		board[8][0] = new blank(8,0," a");
		board[8][1] = new blank(8,0," b");
		board[8][2] = new blank(8,0," c");
		board[8][3] = new blank(8,0," d");
		board[8][4] = new blank(8,0," e");
		board[8][5] = new blank(8,0," f");
		board[8][6] = new blank(8,0," g");
		board[8][7] = new blank(8,0," h");

		board[0][8] = new blank(8,0,"8");
		board[1][8] = new blank(8,0,"7");
		board[2][8] = new blank(8,0,"6");
		board[3][8] = new blank(8,0,"5");
		board[4][8] = new blank(8,0,"4");
		board[5][8] = new blank(8,0,"3");
		board[6][8] = new blank(8,0,"2");
		board[7][8] = new blank(8,0,"1");
		board[8][8] = new blank(8,0," ");

		return board;
	}
	/***
	 * setBoard puts the correct pieces in the correct places on the chess board before any moves are made. It is called on our current chess board at the start of the program.
	 * @param board
	 */
	public static void setBoard(chessPiece [][] board)
	{
		//Set Black Pawns
		board[1][0] = new pawn(1,0,"bp");
		board[1][1] = new pawn(1, 1, "bp");
		board[1][2] = new pawn(1, 2, "bp");
		board[1][3] = new pawn(1, 3, "bp");
		board[1][4] = new pawn(1, 4,"bp");
		board[1][5] = new pawn(1, 5, "bp");
		board[1][6] = new pawn(1, 6, "bp");
		board[1][7] = new pawn(1, 7, "bp"); 
		//
		//		//Set White Pawns 
		board[6][0] = new pawn(6,0,"wp");
		board[6][1] = new pawn(6, 1, "wp");
		board[6][2] = new pawn(6, 2, "wp");
		board[6][3] = new pawn(6, 3, "wp");
		board[6][4] = new pawn(6, 4, "wp");
		board[6][5] = new pawn(6, 5, "wp");
		board[6][6] = new pawn(6, 6, "wp");
		board[6][7] = new pawn(6, 7, "wp"); 
		//Set Rooks
		board[7][7] = new rook(7,7,"wR");
		board[7][0] = new rook(7,0,"wR");
		board[0][7] = new rook(0,7,"bR");
		board[0][0] = new rook(0,0,"bR");
		//Set knight
		board[7][6] = new knight(7,6,"wN");
		board[7][1] = new knight(7,1,"wN");
		board[0][6] = new knight(0,6,"bN");
		board[0][1] = new knight(0,1,"bN");
		//		//Set Bishop
		board[7][5] = new bishop(7,5,"wB");
		board[7][2] = new bishop(7,2,"wB");
		board[0][5] = new bishop(0,5,"bB");
		board[0][2] = new bishop(0,2,"bB");
		//Set King
		board[7][4] = new king(7,4,"wK");
		board[0][4] = new king(0,4,"bK");
		//Set Queen
		board[7][3] = new queen(7,3,"wQ");
		board[0][3] = new queen(0,3,"bQ");
		//Extra Pieces
		//		board[6][5] = new rook(6,5,"wR");




	}
	/***
	 * findWhiteKing finds the white king on the chess board so we can decide whether or not the white king is in check. The return value is a king which is then passed into "isCheck" to see if the king is in danger. 
	 * @param board
	 * @return returns the location of the white king
	 */
	public static king findWhiteKing(chessPiece [][] board) //use to find the king to call inCheck on. Call after every move
	{
		for(int i = 0; i < 8; i++)
		{
			for(int j = 0; j < 8; j++)
			{
				if(board[i][j].color == "wK")
				{
					return (king)board[i][j];
				}
			}
		}
		return null;
	}
	/***
	 * findBlackKing finds the black king on the chess board so we can decide whether or not the white king is in check. The return value is a king which is then passed into "isCheck" to see if the king is in danger. 
	 * @param board
	 * @return returns the location of the black king to be used to check if it is in check
	 */
	public static king findBlackKing(chessPiece [][] board) //use to find the king to call inCheck on. Call after every move
	{
		for(int i = 0; i < 8; i++)
		{
			for(int j = 0; j < 8; j++)
			{
				if(board[i][j].color == "bK")
				{
					return (king)board[i][j];
				}
			}
		}
		return null;
	}
	/***
	 * isCheckMate calls "inCheck" on all of the surrounding spaces of king. If all of those spaces are return true, the king is in check mate and cannot move.  
	 * Checks to see if the piece causing the check can be killed. 
	 * Checks to see if the piece causing check can be blocked. 
	 * @param board
	 * @param K
	 * @return returns a boolean regarding whether the king appears to be in check mate 
	 */
	public static boolean isCheckMate(chessPiece [][] board, king K) // call this after you notice that the king is in check on previous line of game.java
	{
		boolean check1 = true; //initialize to true so if the spot we are checking does not exist, it will still be counted as a true in the final if statement 
		boolean check2 = true;
		boolean check3 = true;
		boolean check4 = true;
		boolean check5 = true;
		boolean check6 = true;
		boolean check7 = true;
		boolean check8 = true;

		if(K.row + 1 <= 7 && (board[K.row+1][K.column].color == "##" || board[K.row+1][K.column].color == "  " || board[K.row+1][K.column].color.charAt(0) != K.color.charAt(0)))
		{
			String x = board[K.row+1][K.column].color;
			board[K.row+1][K.column].color = K.color;
			check1 = inCheck(board, board[K.row+1][K.column], K.color.charAt(0));
			board[K.row+1][K.column].color = x;
		}
		if(K.row - 1 >= 0 && (board[K.row-1][K.column].color == "##" || board[K.row-1][K.column].color == "  " || board[K.row -1][K.column].color.charAt(0) != K.color.charAt(0)))
		{
			char color = K.color.charAt(0);
			String x = board[K.row-1][K.column].color;
			String y = board[K.row][K.column].color;
			board[K.row-1][K.column].color = K.color;
			board[K.row][K.column].color = "  ";

			check2 = inCheck(board,board[K.row-1][K.column], color);
			board[K.row-1][K.column].color = x;
			board[K.row][K.column].color = y;
		}
		if(K.column + 1 <= 7 && (board[K.row][K.column +1].color == "##" || board[K.row][K.column + 1].color == "  " || board[K.row][K.column+1].color.charAt(0) != K.color.charAt(0)))
		{
			char color = K.color.charAt(0);
			String x = board[K.row][K.column + 1].color;
			String y = board[K.row][K.column].color;
			board[K.row][K.column+1].color = K.color;
			board[K.row][K.column].color = "  ";
			check3 = inCheck(board,board[K.row][K.column+1], color);
			board[K.row][K.column].color = y;
			board[K.row][K.column + 1].color = x;
		}
		if(K.column - 1 >= 0 && (board[K.row][K.column -1].color == "##" || board[K.row][K.column - 1].color == "  "|| board[K.row][K.column-1].color.charAt(0) != K.color.charAt(0)))
		{
			char color = K.color.charAt(0);
			String x = board[K.row][K.column - 1].color;
			String y = board[K.row][K.column].color;
			board[K.row][K.column-1].color = K.color;
			board[K.row][K.column].color = "  ";
			check4 = inCheck(board, board[K.row][K.column-1], color);
			board[K.row][K.column].color = y;
			board[K.row][K.column - 1].color = x;
		}
		if(K.row+1 <= 7 && K.column+1 <= 7 && (board[K.row+1][K.column +1].color == "##" || board[K.row+1][K.column + 1].color == "  " || board[K.row+1][K.column+1].color.charAt(0) != K.color.charAt(0)))
		{
			char color = K.color.charAt(0);
			String x = board[K.row+ 1][K.column + 1].color;
			String y = board[K.row][K.column].color;
			board[K.row+1][K.column+1].color = K.color;
			board[K.row][K.column].color = "  ";
			check5 = inCheck(board, board[K.row+1][K.column+1], color);
			board[K.row][K.column].color = y;
			board[K.row+ 1][K.column + 1].color = x;
		}
		if(K.row - 1 >=0 && K.column - 1 >= 0 && (board[K.row - 1][K.column - 1].color == "##" || board[K.row-1][K.column - 1].color == "  " ||board[K.row-1][K.column-1].color.charAt(0) != K.color.charAt(0)))
		{
			char color = K.color.charAt(0);
			String x = board[K.row - 1][K.column - 1].color;
			String y = board[K.row][K.column].color;
			board[K.row-1][K.column-1].color = K.color;
			board[K.row][K.column].color = "  ";
			check6 = inCheck(board, board[K.row-1][K.column-1], color);
			board[K.row][K.column].color = y;
			board[K.row - 1][K.column - 1].color = x;
		}
		if(K.row + 1 <= 7 && K.column - 1 >= 0 && (board[K.row+1][K.column - 1].color == "##" || board[K.row+1][K.column - 1].color == "  " || board[K.row+1][K.column-1].color.charAt(0) != K.color.charAt(0)))
		{
			char color = K.color.charAt(0);
			String x = board[K.row + 1][K.column - 1].color;
			String y = board[K.row][K.column].color;
			board[K.row+1][K.column-1].color = K.color;
			board[K.row][K.column].color = "  ";
			check7 = inCheck(board, board[K.row+1][K.column-1], color);
			board[K.row][K.column].color = y;
			board[K.row + 1][K.column - 1].color = x;
		}
		if(K.row - 1 >= 0 && K.column + 1 <= 7 && (board[K.row-1][K.column + 1].color == "##" || board[K.row - 1][K.column + 1].color == "  " || board[K.row-1][K.column+1].color.charAt(0) != K.color.charAt(0)))
		{
			char color = K.color.charAt(0);
			String x = board[K.row - 1][K.column + 1].color;
			String y = board[K.row][K.column].color;
			board[K.row-1][K.column+1].color = K.color;
			board[K.row][K.column].color = "  ";
			check8 = inCheck(board, board[K.row-1][K.column + 1], color);
			board[K.row][K.column].color = y;
			board[K.row - 1][K.column + 1].color = x;
		}
		if(check8 && check7 && check6 && check5 && check4 && check3 && check2 && check1){
			return true;
		}
		else
		{
			return false;
		}
	}
	/***
	 * inCheck is called on our current board and takes the king return value from "findWhiteKing" or "findBlackKing". 
	 * this method goes through every piece of the opposing function and decides whether the king is in check or not based on calculations of all possible moves by the other team.  
	 * if the king is in check it returns a "true" but otherwise returns "false"
	 * @param board
	 * @param K
	 * @return returns a boolean true if the king passed in is in check and false otherwise
	 */
	public static boolean inCheck(chessPiece [][] board, chessPiece K, char color)
	{
		boolean Check;
		//char color = K.color.charAt(0);
		for(int i = 0; i < 8; i++)
		{
			for( int j = 0; j < 8; j++)
			{
				if(board[i][j].color.charAt(0) == color)
				{

				}
				else
				{
					if(board[i][j].color.charAt(1) == 'p') //Checking pawns
					{

						if(i + 1 < 8 && j + 1 < 8 && board[i+1][j+1] == K && board[i][j].color == "bp") // Check to see if in Check by black pawn on right side
						{
							Check = true;
							board[i][j].causingCheck = true;
							//System.out.println("Check");
							return Check;
						}
						if(i + 1 < 8 && j - 1 >= 0 && board[i+1][j-1] == K && board[i][j].color == "bp") // Check to see if in Check by black pawn on left side 
						{
							Check = true;
							board[i][j].causingCheck = true;
							//System.out.println("Check");
							return Check;
						}
						if(i - 1>= 0 && j + 1 < 8 && board[i-1][j+1] == K && board[i][j].color == "wp")
						{
							Check = true;
							board[i][j].causingCheck = true;
							//System.out.println("Check");
							return Check;
						}
						if(i - 1 >= 0 && j - 1 >= 0 && board[i -1][j-1] == K && board[i][j].color == "wp")
						{
							Check = true;
							board[i][j].causingCheck = true;
							//System.out.println("Check");
							return Check;
						}
					}
					if(board[i][j].color.charAt(1) == 'R')
					{
						if(K.row != i && K.column != j)
						{

						}
						else
						{
							boolean noCheck = false;
							if(K.row == i && K.column > j) // when the row is the same and it king is at a higher column
							{

								for(int x = j +1; x < K.column; x++)
								{
									if(board[i][x].color != "##" && board[i][x].color != "  ")
									{
										if(board[i][x] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											//System.out.println("Check");
											return Check;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									Check = true;
									board[i][j].causingCheck = true;
									//System.out.println("Check");
									return Check;
								}
							}
							if(K.row == i && K.column < j) // when the row is the same and the king is at a lower column 
							{
								for(int x = j -1; x > K.column; x --)
								{
									if(board[i][x].color != "##" && board[i][x].color != "  ")
									{
										if(board[i][x] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											//System.out.println("Check");
											return Check;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									Check = true;
									board[i][j].causingCheck = true;
									//System.out.println("Check");
									return Check;
								}
							}
							if(K.column == j && K.row > i) // King is in the same column as rook and at a higher row 
							{
								for(int x = i +1; x <= K.row ; x++)
								{
									if(board[x][j].color != "##" && board[x][j].color != "  ")
									{
										if(board[x][j] == K)
										{
											//System.out.println("Check");
											board[i][j].causingCheck = true;
											Check = true;
											return Check;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									Check = true;
									//System.out.println("Check");
									board[i][j].causingCheck = true;
									return Check;
								}
							}
							if(K.column == j && K.row < i )
							{
								for(int x = i -1; x > K.row; x--)
								{
									if(board[x][j].color != "##" && board[x][j].color != "  ")
									{
										if(board[x][j] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											return Check;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									Check = true;
									board[i][j].causingCheck = true;
									//System.out.println("Check");
									return Check;
								}
							}
						}
					}
					if(board[i][j].color.charAt(1) == 'Q')
					{
						if(K.row == i || K.column == j) //This case is exactly the same as the rook case;
						{
							boolean noCheck = false;
							if(K.row == i && K.column > j) // when the row is the same and it king is at a higher column
							{

								for(int x = j +1; x < K.column; x++)
								{
									if(board[i][x].color != "##" && board[i][x].color != "  ")
									{
										if(board[i][x] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									Check = true;
									board[i][j].causingCheck = true;
									//System.out.println("Check");
									return Check;
								}
							}
							if(K.row == i && K.column < j) // when the row is the same and the king is at a lower column 
							{
								for(int x = j -1; x > K.column; x --)
								{
									if(board[i][x].color != "##" && board[i][x].color != "  ")
									{
										if(board[i][x] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									Check = true;
									board[i][j].causingCheck = true;
									//System.out.println("Check");
									return Check;
								}
							}
							if(K.column == j && K.row > i) // King is in the same column as rook and at a higher row 
							{
								for(int x = i +1; x <= K.row ; x++)
								{
									if(board[x][j].color != "##" && board[x][j].color != "  ")
									{
										if(board[x][j] == K)
										{
											//System.out.println("Check");
											board[i][j].causingCheck = true;
											Check = true;
											return Check;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									Check = true;
									board[i][j].causingCheck = true;
									//System.out.println("Check");
									return Check;
								}
							}
							if(K.column == j && K.row < i )
							{
								for(int x = i -1; x > K.row; x--)
								{
									if(board[x][j].color != "##" && board[x][j].color != "  ")
									{
										if(board[x][j] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											return Check;
										}
										else{
											noCheck = true;
											break;
										}

									}

								}
								if(noCheck == false)
								{
									Check = true;
									board[i][j].causingCheck = true;
									//System.out.println("Check");
									return Check;
								}
							}
						}


						int x = K.row - i;
						int y = K.column - j;
						x = Math.abs(x);
						y = Math.abs(y);
						if(x == y) //bishop case
						{
							if(K.row > i && K.column > j) //  king is down and too the right of bishop
							{
								int a;
								int b;
								for(a = i + 1, b = j + 1; b <= K.column && a <= K.row ; a++, b++)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											//System.out.println("Check");
											return Check;
										}
										else
										{
											Check = false;
											break;
										}
									}
								}


							}
							if(K.row < i && K.column > j) //king is up and to the  right
							{
								int a;
								int b;
								for( a = i- 1, b = j + 1; b <= K.column && a >= K.row ; a--, b++)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											//System.out.println("Check");
											return Check;
										}
										else
										{
											Check = false;
											break;
										}
									}
								}

							}
							if(K.row > i && K.column < j) // king is down and to the left
							{
								int a;
								int b;
								for(a = i + 1, b = j - 1; b <= j && a >= i ; a++, b--)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											//System.out.println("Check");
											return Check;
										}
										else
										{
											Check = false;
											break;
										}
									}
								}
							}
							if(K.row < i && K.column < j) // king is up and to the  left
							{
								int a;
								int b;
								for(a =i - 1, b = j - 1; b >= K.column && a >= K.row ; a--, b--)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											//System.out.println("Check");
											return Check;
										}
										else
										{
											Check = false;
											break;
										}
									}
								}							
							}

						}
					}
					if(board[i][j].color.charAt(1) == 'N')
					{
						if(Math.abs(K.row - i) > 2 || Math.abs(K.column - j) > 2)
						{

						}
						else
						{


							if(i+2 <= 7 && j + 1 <= 7 && board[i+2][j+1] == K)
							{
								Check = true;
								board[i][j].causingCheck = true;
								//System.out.println("Check");
								return Check;
							}
							if(i+1 <= 7 && j + 2 <= 7 && board[i + 1][j + 2] == K)
							{
								Check = true;
								board[i][j].causingCheck = true;
								//System.out.println("Check");
								return Check;
							}
							if(i+2 <= 7 && j - 1 >= 0 && board[i + 2][j - 1] == K)
							{
								Check = true;
								board[i][j].causingCheck = true;
								//System.out.println("Check");
								return Check;
							}
							if(i+1 <= 7 && j - 2 >= 0 &&board[i + 1][j -2] == K)
							{
								Check = true;
								board[i][j].causingCheck = true;
								//System.out.println("Check");
								return Check;
							}
							if(i -1 >= 0 && j + 2 <= 7 &&board[i-1][j + 2] == K)
							{
								Check = true;
								board[i][j].causingCheck = true;
								//System.out.println("Check");
								return Check;
							}
							if(i-1 >= 0 && j - 2 >= 0 &&board[i-1][j - 2] == K)
							{
								Check = true;
								board[i][j].causingCheck = true;
								//System.out.println("Check");
								return Check;
							}
							if(i-2 >= 0 && j - 1 >= 0 &&board[i-2][j-1] == K)
							{
								Check = true;
								board[i][j].causingCheck = true;
								//System.out.println("Check");
								return Check;
							}
							if(i - 2 >=0 && j + 1 <=7 && board[i-2][j+1] == K)
							{
								Check = true;
								board[i][j].causingCheck = true;
								//System.out.println("Check");
								return Check;
							}
						}

					}
					if(board[i][j].color.charAt(1) == 'B')
					{
						int x = K.row - i;
						int y = K.column - j;
						x = Math.abs(x);
						y = Math.abs(y);
						if(x!=y) 
						{

						}
						else
						{

							if(K.row > i && K.column > j) //  king is down and too the right of bishop
							{
								int a;
								int b;
								for(a = i + 1, b = j + 1; b <= K.column && a <= K.row ; a++, b++)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											//System.out.println("Check");
											return Check;
										}
										else
										{
											Check = false;
											break;
										}
									}
								}


							}
							if(K.row < i && K.column > j) //king is up and to the  right
							{
								int a;
								int b;
								for( a = i- 1, b = j + 1; b <= K.column && a >= K.row ; a--, b++)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											//System.out.println("Check");
											return Check;
										}
										else
										{
											Check = false;
											break;
										}
									}
								}

							}
							if(K.row > i && K.column < j) // king is down and to the left
							{
								int a;
								int b;
								for(a = i + 1, b = j - 1; b <= j && a >= i ; a++, b--)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											//System.out.println("Check");
											return Check;
										}
										else
										{
											Check = false;
											break;
										}
									}
								}
							}
							if(K.row < i && K.column < j) // king is up and to the  left
							{
								int a;
								int b;
								for(a =i - 1, b = j - 1; b >= K.column && a >= K.row ; a--, b--)
								{
									if(board[a][b].color != "##" && board[a][b].color != "  ")
									{
										if(board[a][b] == K)
										{
											Check = true;
											board[i][j].causingCheck = true;
											//System.out.println("Check");
											return Check;
										}
										else
										{
											Check = false;
											break;
										}
									}
								}							
							}

						}
					}
				}
			}
		}
		return false;
	}

	/***
	 * Block check takes in the chess board and a King in check. It checks to see if the path of the piece causing check can be blocked by any
	 * piece on the opposing team. If the check can be blocked, this invalidates the posibility of a check mate happening. Only called when there appears to be a check mate.
	 * @param board
	 * @param K
	 * @return boolean that is true when check cannot be blocked and false when check can be blocked
	 */
	public static boolean blockCheck(chessPiece[][] board, king K)
	{
		for(int i = 0; i <= 7; i ++)
		{
			for(int j = 0; j <= 7; j++)
			{
				if(board[i][j].causingCheck == true && board[i][j].color != K.color)
				{
					if(board[i][j].color.charAt(1) == 'p')
					{
						return false;
					}
					if(board[i][j].color.charAt(1) == 'R')
					{
						if(i == K.row) //int same row 
						{
							if(j > K.column)
							{
								for(int x = j-1; x > K.column; x--)
								{
									String s = board[x][j].color;
									if(board[x][j].color == "  " || board[x][j].color == "##")
									{
										board[x][j].color = "XX";
									}
									boolean temp = board[x][j].causingCheck;
									if(inCheck(board, board[i][x], board[i][j].color.charAt(0)))
									{
										board[x][j].causingCheck = temp;
										board[x][j].color = s;
										//board = board1;
										return false;
									}
									board[x][j].color = s;
								}
							}
							if(j < K.column)
							{
								for(int x = j; x < K.column; x++)
								{
									String s = board[x][j].color;
									if(board[x][j].color == "  " || board[x][j].color == "##")
									{
										board[x][j].color = "XX";
									}
									boolean temp = board[x][j].causingCheck;
									if(inCheck(board, board[i][x], board[i][j].color.charAt(0)))
									{
										//board[i][j].causingCheck = false;
										board[x][j].causingCheck = temp;
										board[x][j].color = s;
										return false;
									}
									board[x][j].color = s;
								}
							}

						}

						if(j == K.column) //in same column 
						{
							if(i < K.row)
							{
								for(int x = i+1; x < K.row; x++)
								{
									String s = board[x][j].color;
									if(board[x][j].color == "  " || board[x][j].color == "##")
									{
										board[x][j].color = "XX";
									}

									boolean temp = board[x][j].causingCheck;
									if(inCheck(board, board[x][j], board[i][j].color.charAt(0)))
									{
										board[x][j].color = s;
										board[x][j].causingCheck = temp;
										return false;
									}
									board[x][j].color = s;
								}

							}
							if(i > K.row)
							{
								for(int x = i-1; x > K.row; x--)
								{
									String s = board[x][j].color;

									if(board[x][j].color == "  " || board[x][j].color == "##")
									{
										board[x][j].color = "XX";
									}
									boolean temp = board[x][j].causingCheck;
									if(inCheck(board, board[x][j], board[i][j].color.charAt(0)))
									{
										board[x][j].color = s;
										board[i][j].causingCheck = temp;
										return false;
									}
									board[x][j].color = s;
								}
							}
						}

					}
					if(board[i][j].color.charAt(1) == 'B')
					{
						if(i> K.row && j > K.column)
						{
							for(int x = i+1, y = j+1; i< K.row && j<K.column; i++, j++)
							{
								String s = board[x][y].color;
								if(board[x][y].color == "  " || board[x][y].color == "##")
								{
									board[x][y].color = "XX";
								}
								boolean temp = board[x][y].causingCheck;
								if(inCheck(board, board[x][y], board[i][j].color.charAt(0)))
								{
									board[x][y].color = s;
									board[x][y].causingCheck = temp;
									//board[i][j].causingCheck = false;
									return false;
								}
								board[x][y].color = s;
							}
						}
						if(i > K.row && j < K.column)
						{
							for(int x = i+1, y = j-1; x < K.row && j > K.column; x++, j--)
							{
								String s = board[x][y].color;
								if(board[x][y].color == "  " || board[x][y].color == "##")
								{
									board[x][y].color = "XX";
								}
								boolean temp = board[x][y].causingCheck;
								if(inCheck(board, board[x][y], board[i][j].color.charAt(0)))
								{
									board[x][y].color = s;
									board[x][y].causingCheck = temp;
									//board[i][j].causingCheck = false;
									return false;
								}
								board[x][y].color = s;
							}
						}
						if(i < K.row && j >K.column)
						{
							for(int x = i -1, y = j+1; x > K.row && y < K.column; i--, j++)
							{
								String s = board[x][y].color;
								if(board[x][y].color == "  " || board[x][y].color == "##")
								{
									board[x][y].color = "XX";
								}
								boolean temp = board[x][y].causingCheck;
								if(inCheck(board, board[x][y], board[i][j].color.charAt(0)))
								{
									board[x][y].color = s;
									board[x][y].causingCheck = temp;								
									return false;
								}
								board[x][y].color = s;
							}
						}
						if(i < K.row && j < K.column)
						{
							for(int x = i-1, y = j - 1; x > K.row && y < K.column; i--, j-- )
							{
								String s = board[x][y].color;
								if(board[x][y].color == "  " || board[x][y].color == "##")
								{
									board[x][y].color = "XX";
								}
								boolean temp = board[x][y].causingCheck;
								if(inCheck(board, board[x][y], board[i][j].color.charAt(0)))
								{
									board[x][y].color = s;
									board[x][y].causingCheck = temp;
									return false;
								}
								board[x][y].color = s;
							}
						}

					}
					if(board[i][j].color.charAt(1) == 'Q')
					{
						if(i == K.row || j == K.column) //case where it is like rook
						{
							if(i == K.row) //int same row 
							{
								if(j > K.column)
								{
									for(int x = j-1; x > K.column; x--)
									{
										String s = board[x][j].color;
										if(board[x][j].color == "  " || board[x][j].color == "##")
										{
											board[x][j].color = "XX";
										}
										boolean temp = board[x][j].causingCheck;
										if(inCheck(board, board[i][x], board[i][j].color.charAt(0)))
										{
											board[x][j].causingCheck = temp;
											board[x][j].color = s;
											//board = board1;
											return false;
										}
										board[x][j].color = s;
									}
								}
								if(j < K.column)
								{
									for(int x = j; x < K.column; x++)
									{
										String s = board[x][j].color;
										if(board[x][j].color == "  " || board[x][j].color == "##")
										{
											board[x][j].color = "XX";
										}
										boolean temp = board[x][j].causingCheck;
										if(inCheck(board, board[i][x], board[i][j].color.charAt(0)))
										{
											//board[i][j].causingCheck = false;
											board[x][j].causingCheck = temp;
											board[x][j].color = s;
											return false;
										}
										board[x][j].color = s;
									}
								}

							}

							if(j == K.column) //in same column 
							{
								if(i < K.row)
								{
									for(int x = i+1; x < K.row; x++)
									{
										String s = board[x][j].color;
										if(board[x][j].color == "  " || board[x][j].color == "##")
										{
											board[x][j].color = "XX";
										}

										boolean temp = board[x][j].causingCheck;
										if(inCheck(board, board[x][j], board[i][j].color.charAt(0)))
										{
											board[x][j].color = s;
											board[x][j].causingCheck = temp;
											return false;
										}
										board[x][j].color = s;
									}

								}
								if(i > K.row)
								{
									for(int x = i-1; x > K.row; x--)
									{
										String s = board[x][j].color;

										if(board[x][j].color == "  " || board[x][j].color == "##")
										{
											board[x][j].color = "XX";
										}
										boolean temp = board[x][j].causingCheck;
										if(inCheck(board, board[x][j], board[i][j].color.charAt(0)))
										{
											board[x][j].color = s;
											board[i][j].causingCheck = temp;
											return false;
										}
										board[x][j].color = s;
									}
								}
							}

						}
						else //Bishop Case  
						{

							if(i> K.row && j > K.column)
							{
								for(int x = i+1, y = j+1; i< K.row && j<K.column; i++, j++)
								{
									String s = board[x][y].color;
									if(board[x][y].color == "  " || board[x][y].color == "##")
									{
										board[x][y].color = "XX";
									}
									boolean temp = board[x][y].causingCheck;
									if(inCheck(board, board[x][y], board[i][j].color.charAt(0)))
									{
										board[x][y].color = s;
										board[x][y].causingCheck = temp;
										//board[i][j].causingCheck = false;
										return false;
									}
									board[x][y].color = s;
								}
							}
							if(i > K.row && j < K.column)
							{
								for(int x = i+1, y = j-1; x < K.row && j > K.column; x++, j--)
								{
									String s = board[x][y].color;
									if(board[x][y].color == "  " || board[x][y].color == "##")
									{
										board[x][y].color = "XX";
									}
									boolean temp = board[x][y].causingCheck;
									if(inCheck(board, board[x][y], board[i][j].color.charAt(0)))
									{
										board[x][y].color = s;
										board[x][y].causingCheck = temp;
										//board[i][j].causingCheck = false;
										return false;
									}
									board[x][y].color = s;
								}
							}
							if(i < K.row && j >K.column)
							{
								for(int x = i -1, y = j+1; x > K.row && y < K.column; i--, j++)
								{
									String s = board[x][y].color;
									if(board[x][y].color == "  " || board[x][y].color == "##")
									{
										board[x][y].color = "XX";
									}
									boolean temp = board[x][y].causingCheck;
									if(inCheck(board, board[x][y], board[i][j].color.charAt(0)))
									{
										board[x][y].color = s;
										board[x][y].causingCheck = temp;								
										return false;
									}
									board[x][y].color = s;
								}
							}
							if(i < K.row && j < K.column)
							{
								for(int x = i-1, y = j - 1; x > K.row && y < K.column; i--, j-- )
								{
									String s = board[x][y].color;
									if(board[x][y].color == "  " || board[x][y].color == "##")
									{
										board[x][y].color = "XX";
									}
									boolean temp = board[x][y].causingCheck;
									if(inCheck(board, board[x][y], board[i][j].color.charAt(0)))
									{
										board[x][y].color = s;
										board[x][y].causingCheck = temp;
										return false;
									}
									board[x][y].color = s;
								}
							}
						}
					}
				}
			}
		}
		return true;

	}
	/***
	 * Kill Check checks to see if the piece causing check can be killed. This is the last way a piece can get out of a check mate. 
	 * this check is done after "isCheckMate" returns true and "blockCheck" returns true. 
	 * @param board
	 * @return boolean telling whether the piece causing check can be blocked 
	 */

	public static boolean killCheck(chessPiece[][] board)
	{
		for(int i = 0; i <= 7; i ++)
		{
			for(int j = 0; j <= 7; j++)
			{
				if(board[i][j].causingCheck == true)
				{
					if(inCheck(board, board[i][j], board[i][j].color.charAt(0)) == true)
					{
						return true;
					}
				}
			}

		}

		return false; 
	}
}

